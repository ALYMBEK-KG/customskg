import { isMaster } from "/lib/xp/cluster";
import { run } from "/lib/xp/context";
import { create as createProject, get as getProject } from "/lib/xp/project";
import { executeFunction } from "/lib/xp/task";

const projects = [
	{
		id: "ky",
		language: "ky",
		displayName: "KY",
		siteConfig: [
			{
				applicationKey: app.name,
			},
		],
		readAccess: {
			public: true,
		},
	},
	{
		id: "ru",
		language: "ru",
		displayName: "RU",
		parent: "ky",
		siteConfig: [
			{
				applicationKey: app.name,
			},
		],
		readAccess: {
			public: true,
		},
	},
	{
		id: "en",
		language: "en",
		displayName: "EN",
		parent: "ky",
		siteConfig: [
			{
				applicationKey: app.name,
			},
		],
		readAccess: {
			public: true,
		},
	},
];

const initializeProject = () => {
	try {
		projects.map((project) => {
			run(
				{
					principals: ["role:system.admin"],
					repository: `com.enonic.cms.${project.id}`,
				},
				() => {
					const existProject = getProject({ id: project.id });

					if (!existProject) {
						log.info(`Project "${project.id}" not found. Creating...`);
						executeFunction({
							description: `Importing "${project.id}" content`,
							func: () => {
								const createdProject = createProject(project);
								if (createdProject) {
									log.info(`Project "${createdProject.id}" successfully created`);
								} else {
									log.error(`Project "${project.id}" create failed`);
								}
							},
						});
					} else {
						log.info(`Project "${existProject.id}" exists, skipping import`);
					}
				}
			);
		});
	} catch (e) {
		log.error(`Error: ${e.message}`);
	}
};

if (isMaster()) {
	initializeProject();
}
