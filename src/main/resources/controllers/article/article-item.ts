import type { Enonic } from "@enonic/js-utils/types/Request";
import type { ComponentProps } from "/react4xp/entries/article-item.d";
import type { ArticleData } from "/site/content-types/article/article.d";

import { render } from "/lib/enonic/react4xp";
import { Content, get as getContentBy, getSiteConfig } from "/lib/xp/content";
import { getPhrases, getSupportedLocales } from "/lib/xp/i18n";
import { assetUrl, attachmentUrl, getContent, imageUrl, pageUrl, processHtml, url } from "/lib/xp/portal";
import { YANDEX_METRIKA_ID, getCache } from "/react4xp/shared/utils/server";
import { get as getLinks } from "/site/content-types/link/link";

export function get(request: Enonic.Xp.Http.Request) {
	const content = getContent<Content<ArticleData>>();
	const cache = getCache();
	const { mode, branch } = request;

	const sitePath = content._path
		.split("/")
		.filter((path) => path)
		.reduce((a, v, i) => {
			let path = `/${v}`;

			if (i > 0) {
				path = `${a[i - 1].path}${path}`;
			}

			a[i] = {
				title: getContentBy({ key: path }).displayName,
				path,
				link: pageUrl({ path }),
			};

			return a;
		}, []);

	const siteData = cache.get(`siteData_${content.language}_${branch}`, () => {
		const currentSiteData = getSiteConfig({ key: `/${content._path}`, applicationKey: app.name });
		const siteData = getSiteConfig({ key: sitePath[0].path, applicationKey: app.name });

		Object.keys(siteData).map((key) => {
			if (currentSiteData[key] != null) siteData[key] = currentSiteData[key];
		});

		if (siteData.logo != null) {
			siteData.logo = imageUrl({
				id: siteData.logo as string,
				scale: "width(720)",
			});
		}

		return siteData as Record<string, string>;
	});

	const navMenuList: ComponentProps["appData"]["navMenuList"] = getLinks({
		pageSize: -1,
		isAll: true,
		filters: [
			{
				hasValue: {
					field: "data.groups",
					values: ["header", "footer"],
				},
			},
		],
	});

	content.data.contentTypeImage =
		content.attachments?._thumbnail?.name != null
			? attachmentUrl({
					id: content._id,
					name: content.attachments._thumbnail.name,
				})
			: "";
	content.data.contentTypeImageAltText = content.attachments?._thumbnail?.name ?? "";
	content.data.content = processHtml({ value: content.data.content });

	const props: ComponentProps = {
		appData: {
			appName: app.name,
			sitePath,
			siteData,
			locale: content.language,
			locales: getSupportedLocales(["i18n/phrases"]).map((locale) => ({
				locale,
				link: url({
					path: `${mode === "preview" ? "/admin" : ""}/site${
						mode === "preview" ? "/" + mode : ""
					}/${locale}/${branch + content._path}`,
				}),
			})),
			phrases: getPhrases(content.language, ["i18n/phrases"]),
			rootPage: pageUrl({ path: "/" }),
			currentPage: pageUrl({ path: content._path }),
			navMenuList,
			isHeaderTransparent: false,
			imageLinks: {
				kgOrnament: assetUrl({ path: "images/kg-ornament.png" }),
				chatIcon: assetUrl({ path: "icons/chat-icon.svg" }),
				doubleCheck: assetUrl({ path: "icons/double-check-icon.svg" }),
				feedbackIcon: assetUrl({ path: "icons/feedback.svg" }),
			},
		},
		item: content,
	};

	const result = render("article-item", props, request, {
		hydrate: true,
		ssr: true,
		id: content._id,
		body: /* HTML */ `<!doctype html>
			<html lang="${content.language}">
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1" />
					<link rel="icon" type="image/x-icon" href="${siteData.logo ?? ""}" />
					<title>${content.displayName}</title>
					${content.page.config?.styles ? `<style>${content.page.config.styles}</style>` : ""}
				</head>
				<body class="xp-page">
					<div id="${content._id}" class="root"></div>

					<!-- Yandex.Metrika counter -->
					<script type="text/javascript">
						(function (m, e, t, r, i, k, a) {
							m[i] =
								m[i] ||
								function () {
									(m[i].a = m[i].a || []).push(arguments);
								};
							m[i].l = 1 * new Date();
							for (var j = 0; j < document.scripts.length; j++) {
								if (document.scripts[j].src === r) {
									return;
								}
							}
							(k = e.createElement(t)),
								(a = e.getElementsByTagName(t)[0]),
								(k.async = 1),
								(k.src = r),
								a.parentNode.insertBefore(k, a);
						})(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

						ym(${YANDEX_METRIKA_ID}, "init", {
							clickmap: true,
							trackLinks: true,
							accurateTrackBounce: true,
							webvisor: true,
							ecommerce: "dataLayer",
						});
					</script>
					<noscript
						><div>
							<img
								src="https://mc.yandex.ru/watch/${YANDEX_METRIKA_ID}"
								style="position:absolute; left:-9999px;"
								alt=""
							/></div
					></noscript>
					<!-- /Yandex.Metrika counter -->
				</body>
			</html>`,
	});

	result.headers = {
		...result.headers,
		"Content-Security-Policy": `
			default-src * data: mediastream: blob: filesystem: about: ws: wss: 'unsafe-eval' 'wasm-unsafe-eval' 'unsafe-inline'; 
			script-src * data: blob: 'unsafe-inline' 'unsafe-eval'; 
			script-src-elem * data: blob: 'unsafe-inline' 'unsafe-eval'; 
			connect-src * data: blob: 'unsafe-inline'; 
			img-src * data: blob: 'unsafe-inline'; 
			media-src * data: blob: 'unsafe-inline'; 
			frame-src * data: blob: ; 
			style-src * data: blob: 'unsafe-inline'; 
			font-src * data: blob: 'unsafe-inline'; 
			frame-ancestors * data: blob:; 
		`,
	};

	return result;
}
