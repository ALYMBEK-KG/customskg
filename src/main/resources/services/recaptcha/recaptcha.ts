import type { Enonic } from "@enonic/js-utils/types/Request";
import { http, RECAPTCHA_SECRET_KEY } from "/react4xp/shared/utils/server";

export const post = function (request: Enonic.Xp.Http.Request) {
	try {
		const secretKey = RECAPTCHA_SECRET_KEY;
		const token = request.params?.token;
		const response = http({
			url: `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${token}`,
			method: "POST",
		});

		const recaptcha = response.status === 200 ? JSON.parse(response.body) : null;

		return {
			status: response.status,
			contentType: "application/json",
			body: recaptcha,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
