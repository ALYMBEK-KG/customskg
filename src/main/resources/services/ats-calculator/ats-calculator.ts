import type { Enonic } from "@enonic/js-utils/types/Request";
import { ADMIN_BASE_URL, http } from "/react4xp/shared/utils/server";

export const post = function (request: Enonic.Xp.Http.Request) {
	try {
		const jsonBody = JSON.parse(request.body ?? "null");

		const response = http({
			url: `/ws/public/calculate/vehicle`,
			method: "POST",
			baseUrl: ADMIN_BASE_URL,
			body: JSON.stringify(jsonBody),
		});

		const jsonResponse = response.status === 200 ? JSON.parse(response.body ?? "null") : null;

		return {
			status: 200,
			contentType: "application/json",
			body: jsonResponse,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
