import type { Enonic } from "@enonic/js-utils/types/Request";
import { ADMIN_BASE_URL, http } from "/react4xp/shared/utils/server";

export const get = function (request: Enonic.Xp.Http.Request) {
	try {
		const response = http({
			url: "/ws/public/vote/check",
			method: "GET",
			baseUrl: ADMIN_BASE_URL,
			params: { ip: request.remoteAddress },
		});

		const rate = response.status === 200 ? JSON.parse(response.body) : null;

		return {
			status: response.status,
			contentType: "application/json",
			body: rate,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};

export const post = function (request: Enonic.Xp.Http.Request) {
	try {
		const jsonBody = {
			ipAddress: request.remoteAddress,
			value: JSON.parse(request.body ?? "null"),
		};
		const response = http({
			url: "/ws/public/vote/vote",
			method: "POST",
			baseUrl: ADMIN_BASE_URL,
			body: JSON.stringify(jsonBody),
		});

		const rate = response.status === 200 ? JSON.parse(response.body) : null;

		return {
			status: response.status,
			contentType: "application/json",
			body: rate,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
