import { ADMIN_BASE_URL, http } from "/react4xp/shared/utils/server";

export const get = function () {
	try {
		const response = http({
			url: "/ws/public/calculate/countries",
			baseUrl: ADMIN_BASE_URL,
		});

		const countries = response.status === 200 ? JSON.parse(response.body ?? "null")?.data : null;

		return {
			status: response.status,
			contentType: "application/json",
			body: countries,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
