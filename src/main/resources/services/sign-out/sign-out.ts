import type { Enonic } from "@enonic/js-utils/types/Request";

import { ADMIN_BASE_URL, getCookie, http } from "/react4xp/shared/utils/server";

export const get = (request: Enonic.Xp.Http.Request) => {
	try {
		const cookie = Array.isArray(request.headers["cookie"])
			? request.headers["cookie"].join(" ")
			: request.headers["cookie"];

		const response = http({
			url: "/logout",
			method: "GET",
			baseUrl: ADMIN_BASE_URL,
			headers: { Cookie: cookie ?? "" },
		});

		if (response.status !== 200) {
			return {
				status: response.status,
				contentType: "application/json",
				body: response.body,
			};
		}

		const newCookie = Array.isArray(response.headers["set-cookie"])
			? response.headers["set-cookie"].join(" ")
			: response.headers["set-cookie"];

		return {
			status: 200,
			contentType: "application/json",
			cookies: {
				JSESSIONID: {
					value: getCookie("JSESSIONID", newCookie),
					path: getCookie("Path", newCookie),
				},
			},
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
