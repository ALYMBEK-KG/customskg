import type { Enonic } from "@enonic/js-utils/types/Request";

import { ADMIN_BASE_URL, getCookie, http } from "/react4xp/shared/utils/server";

export const post = (request: Enonic.Xp.Http.Request) => {
	try {
		const jsonBody = JSON.parse(request.body ?? "null");

		const response = http({
			url: "/callback",
			method: "POST",
			baseUrl: ADMIN_BASE_URL,
			body: JSON.stringify(jsonBody),
		});

		if (response.status !== 200) {
			return {
				status: response.status,
				contentType: "application/json",
				body: response.body,
			};
		}

		const cookie = Array.isArray(response.headers["set-cookie"])
			? response.headers["set-cookie"].join(" ")
			: response.headers["set-cookie"];

		const userResponse = http({
			url: `/ws/rest/com.axelor.auth.db.User/search`,
			method: "POST",
			baseUrl: ADMIN_BASE_URL,
			headers: { Cookie: cookie },
			body: JSON.stringify({
				offset: 0,
				limit: 1,
				fields: ["id", "fullName", "code"],
				data: {
					criteria: [
						{
							fieldName: "code",
							operator: "=",
							value: jsonBody?.username,
						},
					],
				},
			}),
		});

		const user = userResponse.status === 200 ? JSON.parse(userResponse.body ?? "null")?.data?.[0] : null;

		return {
			status: 200,
			contentType: "application/json",
			cookies: {
				JSESSIONID: {
					value: getCookie("JSESSIONID", cookie),
					path: getCookie("Path", cookie),
				},
			},
			body: {
				cookie,
				user,
			},
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
