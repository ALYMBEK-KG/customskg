import { ADMIN_BASE_URL, http } from "/react4xp/shared/utils/server";

export const get = function () {
	try {
		const response = http({
			url: "/ws/public/calculate/units",
			baseUrl: ADMIN_BASE_URL,
		});

		const units = response.status === 200 ? JSON.parse(response.body ?? "null")?.data : null;

		return {
			status: response.status,
			contentType: "application/json",
			body: units,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
