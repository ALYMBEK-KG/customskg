import type { Enonic } from "@enonic/js-utils/types/Request";

import { http, WEATHER_APP_ID } from "/react4xp/shared/utils/server";

export const get = function (request: Enonic.Xp.Http.Request) {
	try {
		const response = http({
			url: "https://api.openweathermap.org/data/2.5/weather",
			queryParams: {
				appid: WEATHER_APP_ID,
				units: "metric",
				lat: request.params?.lat,
				lon: request.params?.lon,
			},
		});

		const weather = response.status === 200 ? JSON.parse(response.body) : null;

		return {
			status: response.status,
			contentType: "application/json",
			body: {
				temp: weather?.main?.temp,
				icon: `https://openweathermap.org/img/wn/${weather?.weather?.[0]?.icon}.png`,
			},
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
