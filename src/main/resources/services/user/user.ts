import type { Enonic } from "@enonic/js-utils/types/Request";

import { ADMIN_BASE_URL, http } from "/react4xp/shared/utils/server";

export const get = (request: Enonic.Xp.Http.Request) => {
	try {
		const pageSize = 12;
		const page = !isNaN(parseInt(request.params?.page)) ? parseInt(request.params?.page) : 1;
		const criteria = [];

		if (request.params?.id) {
			criteria.push({
				fieldName: "id",
				operator: "=",
				value: request.params?.id,
			});
		}

		if (request.params?.code) {
			criteria.push({
				fieldName: "code",
				operator: "=",
				value: request.params?.code,
			});
		}

		const userResponse = http({
			url: `/ws/rest/com.axelor.auth.db.User/search`,
			method: "POST",
			baseUrl: ADMIN_BASE_URL,
			headers: { Cookie: request.params?.cookie },
			body: JSON.stringify({
				offset: page > 0 && pageSize > 0 ? Math.ceil((page - 1) * pageSize) : 0,
				limit: pageSize,
				fields: ["id", "fullName", "code"],
				data: {
					criteria,
				},
			}),
		});

		const users = userResponse.status === 200 ? JSON.parse(userResponse.body)?.data : null;

		return {
			status: userResponse.status,
			contentType: "application/json",
			body: users,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
