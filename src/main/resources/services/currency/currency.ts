import type { Enonic } from "@enonic/js-utils/types/Request";

import { ADMIN_BASE_URL, http } from "/react4xp/shared/utils/server";

export const get = function (request: Enonic.Xp.Http.Request) {
	try {
		const response = http({
			url: "/ws/public/currency/getCurrencyDailyRates",
			baseUrl: ADMIN_BASE_URL,
			queryParams: {
				code: request.params?.code,
			},
		});

		const currency = response.status === 200 ? JSON.parse(response.body ?? "null")?.data : null;

		return {
			status: response.status,
			contentType: "application/json",
			body: currency,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
