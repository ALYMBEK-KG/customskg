import { getSite, pageUrl } from "/lib/xp/portal";

export const handle404 = () => {
	const site = getSite();
	const sitePath = site._path.split("/").filter((path) => path);
	const errorPageUrl = pageUrl({
		path: `/${sitePath[0]}/error404`,
	});

	return {
		redirect: errorPageUrl,
	};
};

export const handleError = (err) => {
	return {
		contentType: "text/html",
		body: /* HTML */ `
			<html>
				<body>
					<h1>Error code "${err.status}"</h1>
				</body>
			</html>
		`,
	};
};
