import type { Content } from "@enonic-types/lib-content/content.d";
import type { ArticleData } from "../../site/content-types/article/article.d";
import type { AppData } from "../shared/types/app-data.d";

export interface ComponentProps {
	appData: AppData;
	item: Content<ArticleData>;
}
