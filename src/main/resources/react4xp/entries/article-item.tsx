import type { ComponentProps } from "./article-item.d";

import dayjs from "dayjs";
import React, { useCallback } from "react";

import { useEffectOnce } from "../shared/hooks";
import Layout from "../shared/layouts/default";
import { useAppStore } from "../shared/stores";
import { http } from "../shared/utils/client";

function Component(props: ComponentProps) {
	const { item } = props;

	const appStore = useAppStore((state) => state);

	const incrementItemViews = useCallback(() => {
		if (appStore.sitePath == null || appStore.appName == null || item == null) return;

		http({
			url: "api/article?incrementViewed",
			method: "PUT",
			body: JSON.stringify({ _id: item._id }),
		});
	}, [appStore.sitePath, appStore.appName, item]);

	useEffectOnce(() => {
		incrementItemViews();
	}, [incrementItemViews]);

	return (
		<Layout {...props}>
			<main className="article-item">
				<div className="container">
					<div className="row my-3">
						<div className="col">
							<h2 className="title">{item?.displayName}</h2>

							<p className="d-flex gap-3 text-muted">
								<span className="d-flex gap-2">
									<i className="fa-solid fa-calendar"></i>
									<small>{dayjs(item?.publish?.from ?? item?.modifiedTime).format("DD.MM.YYYY")}</small>
								</span>

								<span className="d-flex gap-2">
									<i className="fa-solid fa-eye"></i>
									<small>{`${item?.data?.viewed ?? 0}`}</small>
								</span>
							</p>
						</div>
					</div>

					<div className="row my-3">
						<div className="col">
							<div
								dangerouslySetInnerHTML={{
									__html: item?.data?.content,
								}}
							></div>
						</div>
					</div>
				</div>
			</main>
		</Layout>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
