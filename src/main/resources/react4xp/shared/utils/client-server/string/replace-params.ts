export function replaceParams(message: string, ...args: string[]) {
	args.map((arg, i) => {
		message = message.replace(`{${i}}`, arg);
	});

	return message;
}
