export function camelize(data: string[], isPascalCase: boolean = false) {
	let result = "";

	data.map((item, i) => {
		const word = item.toLowerCase();
		const firstLetter = word.charAt(0);
		const remainingLetters = word.slice(1);

		if (i === 0) {
			result += isPascalCase ? firstLetter.toUpperCase() : firstLetter;
			result += remainingLetters;
		} else {
			result += firstLetter.toUpperCase();
			result += remainingLetters;
		}
	});

	return result;
}
