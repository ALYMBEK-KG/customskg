export function getCookie(name: string, cookie: string = null) {
	if (!name) return;

	const cn = name + "=";
	const ca = cookie ? cookie.split(";") : document ? document.cookie.split(";") : [];

	for (let i = 0; i < ca.length; i++) {
		let c = ca[i];

		while (c.charAt(0) === " ") {
			c = c.substring(1);
		}

		if (c.indexOf(cn) === 0) {
			return c.substring(cn.length, c.length);
		}
	}

	return "";
}

export function setCookie(name: string, value: string, exhours: number = 1, path: string = "/") {
	if (!name || !value || !exhours || !path) return;

	const date = new Date();
	const exp = exhours * 60 * 60 * 1000;
	date.setTime(date.getTime() + exp);

	const result = `${name}=${value};expires=${date.toUTCString()};path=${path}`;

	if (document != null) document.cookie = result;

	return result;
}
