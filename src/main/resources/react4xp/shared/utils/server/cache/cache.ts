import { Cache, newCache, NewCacheParams } from "/lib/cache";

const caches: Record<string, Cache> = {};

export type GetCacheOptions = NewCacheParams & {
	name: string;
};

export function getCache(options?: GetCacheOptions): Cache {
	const name = options?.name ?? "general";

	if (caches[name] == null || options != null) {
		caches[name] = newCache({ size: options?.size ?? 150, expire: options?.expire ?? 150 });
	}

	return caches[name];
}
