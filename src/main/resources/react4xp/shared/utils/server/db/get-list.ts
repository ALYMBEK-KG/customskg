import type { LayoutComponent, PageComponent, PartComponent } from "@enonic-types/core";
import type { ContentType } from "../../../types/content-type.d";

import { getCache } from "../cache";
import { Aggregations, Content, ContentsResult, Filter, Highlight, QueryDsl, SortDsl, query } from "/lib/xp/content";
import { attachmentUrl, getComponent, getContent, pageUrl, processHtml } from "/lib/xp/portal";

export type GetListOptions = {
	page?: number;
	pageSize?: number;
	isAll?: boolean;
	group?: string;
	sort?: SortDsl[];
	query?: QueryDsl;
	filters?: Filter[];
	highlight?: Highlight;
	aggregations?: Aggregations;
	contentTypes: string[];
	onEveryItem?: (item: Content) => void;
};

export function getList<
	T extends ContentType & {
		link?: string;
		content?: string;
		[key: string]: unknown;
	},
>(options: GetListOptions): ContentsResult<Content<T>> {
	const content = getContent();
	const component = getComponent<LayoutComponent | PageComponent | PartComponent>();
	const cache = getCache({ name: `get-list_${content?.language}_${content?._name}`, size: 15, expire: 15 });

	let pathMatch = "/content/*";
	if (content != null) {
		const sitePath = content._path.split("/").filter((path) => path);
		pathMatch = `/content${options?.isAll ? `/${sitePath[0]}` : content._path}/*`;
	}

	const _options: typeof options = {
		page: 1,
		pageSize: 12,
		group: component?.config?.group as string,
		sort: [
			{ field: "publish.from", direction: "DESC" },
			{ field: "modifiedTime", direction: "DESC" },
			{ field: "createdTime", direction: "DESC" },
		],
		query: {
			boolean: {
				must: [
					{
						term: {
							field: "valid",
							value: true,
						},
					},
					{
						term: {
							field: "data.active",
							value: true,
						},
					},
					{
						pathMatch: {
							field: "_path",
							path: pathMatch,
							minimumMatch: pathMatch.split("/").filter((path) => path).length - 1,
						},
					},
				],
			},
		},
		...options,
		contentTypes: options.contentTypes.map((item) => `${app.name}:${item}`),
	};

	return cache.get(JSON.stringify(_options), () => {
		const result = query<Content<T>>({
			start: _options.page > 0 && _options.pageSize > 0 ? Math.ceil((_options.page - 1) * _options.pageSize) : 0,
			count: _options.pageSize,
			sort: _options.sort,
			query: _options.query,
			filters: _options.filters,
			highlight: _options.highlight,
			aggregations: _options.aggregations as never,
			contentTypes: _options.contentTypes,
		});

		result.hits = result.hits.map((item) => {
			const sitePath = item._path.split("/").filter((path) => path);

			if ("link" in item.data) {
				item.data.link =
					typeof item.data.link === "string" && item.data.link?.indexOf("http") > -1
						? item.data.link
						: pageUrl({ path: `/${sitePath[0]}/${item.data.link}` });
			}

			if ("content" in item.data) {
				item.data.content = processHtml({ value: item.data.content });
			}

			item.data.contentTypeLink = pageUrl({ id: item._id });

			item.data.contentTypeImage =
				item.attachments?._thumbnail?.name != null
					? attachmentUrl({
							id: item._id,
							name: item.attachments._thumbnail.name,
						})
					: "";

			item.data.contentTypeImageAltText = item.attachments?._thumbnail?.name ?? "";

			if (_options.onEveryItem) _options.onEveryItem(item);

			return item;
		});

		return result;
	});
}
