import { getCache } from "../cache";
import { Content, modify } from "/lib/xp/content";
import { getContent } from "/lib/xp/portal";

import type { ContentType } from "../../../types/content-type.d";

const contentTypeKeys: (keyof ContentType)[] = ["contentTypeLink", "contentTypeImage", "contentTypeImageAltText"];

export function updateItem<T = Record<string, unknown>>(item: Partial<Content<T>>): Content<T> {
	const content = getContent();
	const cache = getCache({ name: `update-item_${content?.language}_${content?._name}`, size: 15, expire: 15 });

	return cache.get(JSON.stringify(item), () => {
		const result = modify<T>({
			key: item._id,
			editor: (c) => {
				if (item?.displayName != null) c.displayName = item.displayName;

				if (item?.data != null) {
					Object.keys(item.data).map((key) => {
						if (contentTypeKeys.indexOf(key as keyof ContentType) < 0 && item.data[key] != null) {
							c.data[key] = item.data[key];
						}
					});
				}

				return c;
			},
		});

		return result;
	});
}
