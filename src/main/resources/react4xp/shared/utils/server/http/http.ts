import { HttpRequestParams, HttpResponse, request } from "/lib/http-client";

export type HttpRequestOptions = HttpRequestParams & {
	baseUrl?: string;
};

export function http(options: HttpRequestOptions): HttpResponse {
	const jsonMimeType = "application/json";
	const url = `${options?.baseUrl ?? ""}${options.url}`;

	return request({
		headers: { Accept: jsonMimeType },
		contentType: jsonMimeType,
		...options,
		url,
	});
}
