export type HttpRequestOptions = RequestInit & {
	url: string;
	baseUrl?: string;
};

export type HttpResponse<T> = Response & {
	jsonData?: T;
};

export async function http<T = unknown>(options: HttpRequestOptions): Promise<HttpResponse<T>> {
	const jsonMimeType = "application/json";
	const url = `${options?.baseUrl ?? ""}${options.url}`;
	const headers = (options.headers as Record<string, string>) ?? {};
	const contentType = headers["Content-Type"];

	if (contentType == null) {
		headers["Content-Type"] = jsonMimeType;
	} else if (contentType === "multipart/form-data") {
		delete headers["Content-Type"];
	}

	options.headers = {
		Accept: jsonMimeType,
		...headers,
	};

	const response = (await fetch(url, options)) as HttpResponse<T>;

	if (response.ok && options.headers["Accept"] === jsonMimeType) {
		response.jsonData = await response.json();
	}

	return response;
}
