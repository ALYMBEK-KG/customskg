import { ObjectSchema } from "yup";

export const isRequiredField = (options: {
	schema: ObjectSchema<unknown>;
	field: string;
	values?: Record<string, unknown>;
}) => {
	const schemaDescription = options.schema.describe({ value: options.values });
	const field = schemaDescription.fields[options.field];

	return !!(field && "optional" in field && field.optional === false);
};

export const fieldErrorParams = (
	options: {
		schema: ObjectSchema<unknown>;
		field: string;
		error: string;
		values?: Record<string, unknown>;
	},
	...args: { name: string; param?: string }[]
) => {
	const schemaDescription = options.schema.describe({ value: options.values });
	const fieldDescription = schemaDescription.fields[options.field];

	args.map((arg, i) => {
		if ("tests" in fieldDescription) {
			const finded = fieldDescription?.tests.find((item) => item.name === arg.name);
			if (finded.params[arg.param ?? arg.name]) {
				options.error = options.error.replace(`{${i}}`, `${finded.params?.[arg.param ?? arg.name] ?? ""}`);
			}
		}
	});

	return options.error;
};
