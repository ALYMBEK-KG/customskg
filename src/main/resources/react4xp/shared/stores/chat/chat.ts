"use client";
import { create } from "zustand";
import { MessageType, UserType, domain, parseTimeStamp } from "../../components/chat/helpers";
import { getCookie } from "../../utils/client-server";

type chatStores = {
	sse: EventSource | null;
	messages: MessageType[];
	setMessages: (newMessage: MessageType[]) => void;
	SseOn: () => void;
};

export const useChatStore = create<chatStores>()((set) => ({
	sse: null,
	messages: [],
	setMessages: (newMessages: MessageType[]) => {
		set({ messages: newMessages });
	},
	SseOn: async () => {
		const cookieValue: string = getCookie("onlineChatUser");
		const onlineChatUser: UserType = cookieValue ? JSON.parse(cookieValue) : undefined;
		if (onlineChatUser) {
			const address = `${domain}/onlineChat?userPhone=${onlineChatUser.userPhone}&userName=${onlineChatUser.userName}&id=${onlineChatUser.id}`;

			const eventChat: EventSource = new EventSource(address, {
				withCredentials: true,
			});
			set({ sse: eventChat });

			eventChat.onopen = function () {};

			eventChat.onmessage = async function (event) {
				const eventData = JSON.parse(event.data);
				const messagesText = JSON.parse(eventData.messagesText);
				if (messagesText.data) {
					set({ messages: getNewMessages(messagesText.data) });
				}
			};

			eventChat.addEventListener("newMessage", async function (event) {
				const eventData = JSON.parse(event.data);
				const messagesText = eventData.messageChat.messagesText;
				const messagesTextParse = JSON.parse(messagesText);
				set({ messages: getNewMessages(messagesTextParse.data) });
			});

			// eventChat.addEventListener("play musics", function (event) {

			// })

			// eventChat.onerror = function (event) {
			// }
		}
	},
}));

function getNewMessages(messages: MessageType[]) {
	const newMessages: MessageType[] = [];
	messages.reverse();
	for (let i = 0; i < messages.length; i++) {
		const message: MessageType = messages[i];
		if (message.timestamp) {
			const messageTimeStamp: { type: string; text: string } = getMessageDate(message.timestamp);
			if (messageTimeStamp.type === "date") {
				const messageTimeStampArr: string[] = messageTimeStamp.text.split(" ");
				const messageDate: string = messageTimeStampArr[0];
				const time: string[] = messageTimeStampArr[1].split(":");
				time.pop();
				message.time = time.join(":");
				const newMessageDate: MessageType | number = newMessages.findIndex(
					(el) => el.type == "date" && el.body === messageDate
				);
				if (newMessageDate === -1) {
					newMessages.push({
						body: messageDate,
						type: "date",
						messageAuthor: "message_date",
						id: Math.random() * 1000,
					});
					newMessages.push(message);
				} else {
					newMessages.push(message);
				}
			} else {
				const today: MessageType | number = newMessages.findIndex((el) => el.type === "today");
				message.time = parseTimeStamp(message.timestamp);
				if (today === -1) {
					newMessages.push({ body: "СЕГОДНЯ", type: "today", messageAuthor: "message_date", id: Math.random() * 1000 });
					newMessages.push(message);
				} else {
					newMessages.push(message);
				}
			}
		} else {
			newMessages.push(message);
		}
	}
	return newMessages.reverse();
}

function getMessageDate(timeStamp: number) {
	const date: string = parseTimeStamp(timeStamp);
	if (date.length > 5) {
		return { type: "date", text: date };
	} else {
		return { type: "time", text: date };
	}
}
