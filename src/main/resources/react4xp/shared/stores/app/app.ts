import { create } from "zustand";
import { persist } from "zustand/middleware";
import { AppData } from "../../types/app-data.d";

const initialStore: AppData = {
	appName: null,
	sitePath: null,
	siteData: null,
	locale: null,
	locales: null,
	phrases: null,
	rootPage: null,
	currentPage: null,
	navMenuList: null,
	alerts: null,
	authData: null,
	imageLinks: null,
	isHeaderTransparent: false,
};

export const useAppStore = create(
	persist<
		AppData & {
			alert: (data: AppData["alerts"][0]) => void;
			update: (items: Partial<AppData>) => void;
			clear: () => void;
		}
	>(
		(set, get) => ({
			...initialStore,
			alert: (data) => {
				const alerts = get().alerts ?? [];
				alerts.push(data);
				set({ alerts });
			},
			update: (items) => {
				set({ ...items });
			},
			clear: () => {
				set(initialStore);
			},
		}),
		{ name: "app" }
	)
);
