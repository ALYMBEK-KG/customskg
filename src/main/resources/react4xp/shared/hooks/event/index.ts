import { useEffect } from "react";

export function useEvent(callback: (e: Event) => void, options?: { selector?: string; type?: string }) {
	useEffect(() => {
		const _options = { selector: "body", type: "click", ...options };

		if (!document || !_options.selector || !_options.type) return;

		document.querySelectorAll(_options.selector)?.forEach((item) => {
			item.addEventListener(_options.type, callback);
		});

		return () => {
			document.querySelectorAll(_options.selector)?.forEach((item) => {
				item.removeEventListener(_options.type, callback);
			});
		};
	}, [options, callback]);
}
