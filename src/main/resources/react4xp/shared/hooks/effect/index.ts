import { DependencyList, useEffect, useId } from "react";

const timers: Record<string, ReturnType<typeof setTimeout>> = {};

// eslint-disable-next-line @typescript-eslint/ban-types
export function useEffectOnce(effect: Function, deps: DependencyList = [], timer = 10) {
	const id = useId();

	useEffect(() => {
		if (timers[id]) clearTimeout(timers[id]);

		timers[id] = setTimeout(() => effect(), timer);

		return () => {
			clearTimeout(timers[id]);
			delete timers[id];
		};

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, deps);
}
