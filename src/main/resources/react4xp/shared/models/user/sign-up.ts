import { bool, date, InferType, number, object, string } from "yup";

export const signUp = object({
	typeSelect: number().oneOf([1, 2]).default(2).required("requiredField"),
	firstName: string()
		.trim()
		.when("typeSelect", {
			is: (value) => value == 2,
			then: (schema) => schema.required("requiredField"),
			otherwise: (schema) => schema.optional(),
		}),
	lastName: string()
		.trim()
		.when("typeSelect", {
			is: (value) => value == 2,
			then: (schema) => schema.required("requiredField"),
			otherwise: (schema) => schema.optional(),
		}),
	birthDate: date().when("typeSelect", {
		is: (value) => value == 2,
		then: (schema) => schema.required("requiredField"),
		otherwise: (schema) => schema.optional(),
	}),
	personalNumber: string()
		.trim()
		.when("typeSelect", {
			is: (value) => value == 2,
			then: (schema) => schema.required("requiredField"),
			otherwise: (schema) => schema.optional(),
		}),
	taxPayerId: string()
		.trim()
		.when("typeSelect", {
			is: (value) => value == 1,
			then: (schema) => schema.required("requiredField"),
			otherwise: (schema) => schema.optional(),
		}),
	email: string().email("invalidEmail").trim().required("requiredField"),
	privacyAgreement: bool().default(true).required("requiredField"),
});

export type SignUp = InferType<typeof signUp>;
