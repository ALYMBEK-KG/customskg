import { InferType, number, object, string } from "yup";

export const user = object({
	id: number().integer().positive().optional(),
	fullName: string().trim().optional(),
	code: string().trim().optional(),
});

export type User = InferType<typeof user>;
