import { bool, InferType, object, string } from "yup";

export const signIn = object({
	username: string().trim().required("requiredField"),
	password: string().trim().required("requiredField"),
	rememberMe: bool().default(false).optional(),
});

export type SignIn = InferType<typeof signIn>;
