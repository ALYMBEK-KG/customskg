import { InferType, number, object, string } from "yup";

export const importCalculator = object({
	tnved: string().required("requiredField").typeError("onlyNumbers"),
	originCountry: string().required("requiredField"),
	departureCountry: string().required("requiredField"),
	currency: string().required("requiredField"),
	customsValue: number().required("requiredField"),
	amount: number().required("requiredField"),
	amountUnits: number().required("requiredField"),
	weight: number().required("requiredField"),
});

export type ImportCalculator = InferType<typeof importCalculator>;
