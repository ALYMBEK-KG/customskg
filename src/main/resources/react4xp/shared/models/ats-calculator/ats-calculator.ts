import { InferType, number, object, string } from "yup";

export const atsCalculator = object({
	currency: string().required("requiredField"),
	engineCapacity: number().required("requiredField"),
	manufactureYear: number().required("requiredField"),
	engineType: string().required("requiredField"),
	calculationType: string().required("requiredField"),
	customsCost: number().required("requiredField"),
});

export type AtsCalculator = InferType<typeof atsCalculator>;
