export type AreaChartItem = {
	name: string;
	date: string;
	value: number;
};
