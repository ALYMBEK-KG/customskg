import "./index.scss";

import type { AreaChartItem } from "./index.d";

import React from "react";
import { Area, AreaChart, CartesianGrid, ResponsiveContainer, Tooltip, TooltipProps, XAxis, YAxis } from "recharts";

import { useAppStore } from "../../../stores";

export default function Component({ data }: { data: AreaChartItem[] }) {
	const appStore = useAppStore((state) => state);

	const CustomTooltip = ({ active, payload }: TooltipProps<number, string>) => {
		if (active && payload && payload.length) {
			return (
				<div className="custom-tooltip">
					<p className="intro">{payload[0].value}</p>
				</div>
			);
		}

		return null;
	};

	return (
		<ResponsiveContainer width="100%" height="100%" minHeight={300}>
			<AreaChart
				data={data}
				margin={{
					top: 10,
					right: 10,
					left: 10,
					bottom: 10,
				}}
			>
				<defs>
					<linearGradient id="chart-bg" x1="0" y1="1" x2="0" y2="0">
						<stop offset="0%" stopColor="#FFFFFF" stopOpacity={0.5} />
						<stop offset="100%" stopColor="#CBDCFF" stopOpacity={0.5} />
					</linearGradient>
					<linearGradient id="area-color" x1="0" y1="1" x2="0" y2="0">
						<stop offset="0%" stopColor="#B5CCFF" stopOpacity={0.5} />
						<stop offset="100%" stopColor="#5C8FFF" stopOpacity={0.5} />
					</linearGradient>
				</defs>
				<CartesianGrid horizontal={false} vertical={false} fill="url(#chart-bg)" />
				<XAxis
					dataKey="name"
					tickFormatter={(tick) => {
						const lowerCaseTick = tick.toLowerCase().slice(0, 3);
						return appStore.phrases?.[lowerCaseTick].slice(0, 3) ?? "";
					}}
				/>
				<YAxis />
				<Tooltip content={<CustomTooltip />} />
				<Area
					type="monotone"
					dataKey="value"
					stroke="#202E4D"
					fill="url(#area-color)"
					activeDot={{ r: 4, fill: "#355DB7" }}
					dot={{
						r: 6,
						stroke: "#355DB7",
						strokeWidth: 2,
						fill: "#FFF",
						fillOpacity: 1,
					}}
				/>
			</AreaChart>
		</ResponsiveContainer>
	);
}
