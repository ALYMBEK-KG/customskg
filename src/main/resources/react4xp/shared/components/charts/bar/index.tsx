import "./index.scss";

import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Bar, BarChart, CartesianGrid, Legend, ResponsiveContainer, Tooltip, XAxis, YAxis } from "recharts";

import type { BarChartItem } from "./index.d";

const BASE_COLORS = ["#d7a700", "#3699dc", "#a267db", "#6671db"];

function generateColors(dataLength: number) {
	const colors = [...BASE_COLORS];
	while (colors.length < dataLength) {
		colors.push(`#${Math.floor(Math.random() * 16777215).toString(16)}`);
	}
	return colors;
}

export default function Component({ data }: { data: BarChartItem[] }) {
	const [opacity, setOpacity] = React.useState<Record<string, number>>({});
	const [groupedData, setGroupedData] = useState({});

	const COLORS = useMemo(() => generateColors(data.length), [data.length]);

	const handleMouseEnter = useCallback(
		(o) => {
			const { dataKey } = o;
			setOpacity((prevOpacity) => ({ ...prevOpacity, [dataKey]: 0.5 }));
		},
		[setOpacity]
	);

	const handleMouseLeave = useCallback(
		(o) => {
			const { dataKey } = o;
			setOpacity((prevOpacity) => ({ ...prevOpacity, [dataKey]: 1 }));
		},
		[setOpacity]
	);

	useEffect(() => {
		setGroupedData(
			data?.reduce((acc, item) => {
				item.values.forEach(({ date, value }) => {
					const year = new Date(date).getFullYear();
					if (!acc[year]) {
						acc[year] = { date: year };
					}
					acc[year][item.name] = value;
				});
				return acc;
			}, {})
		);
	}, [data]);

	return (
		<ResponsiveContainer width="100%" height="100%" minHeight={400}>
			<BarChart
				data={Object.entries(groupedData).map(([year, values]) => ({
					date: year,
					...(values as Record<string, string>),
				}))}
				margin={{ top: 20, right: 10, left: 0, bottom: 0 }}
			>
				<CartesianGrid />
				<XAxis dataKey="date" />
				<YAxis />
				<Tooltip />
				<Legend
					wrapperStyle={{ top: 0 }}
					verticalAlign="top"
					onMouseEnter={handleMouseEnter}
					onMouseLeave={handleMouseLeave}
				/>
				{data?.map((item, idx) => (
					<Bar
						key={item.name}
						dataKey={item.name}
						fill={COLORS[idx]}
						fillOpacity={opacity[item.name]}
						label={{ position: "insideTop", fill: "#fff", fontWeight: 700 }}
					/>
				))}
			</BarChart>
		</ResponsiveContainer>
	);
}
