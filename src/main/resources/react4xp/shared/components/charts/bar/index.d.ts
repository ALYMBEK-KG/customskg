export type BarChartItem = {
	name: string;
	values: {
		date: string;
		value: number;
	}[];
};
