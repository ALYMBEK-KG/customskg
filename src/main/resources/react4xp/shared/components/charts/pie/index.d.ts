export type PieChartItem = {
	name: string;
	date?: string;
	value: number;
};
