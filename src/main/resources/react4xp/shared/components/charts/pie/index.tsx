import type { PieChartItem } from "./index.d";
import "./index.scss";

import React, { FC, useCallback, useMemo, useState } from "react";
import { Cell, Label, LabelProps, Legend, Pie, PieChart, ResponsiveContainer, Tooltip } from "recharts";
import { useAppStore } from "/react4xp/shared/stores";

const BASE_COLORS = ["#6483ff", "#6B7432FF", "#72e3e3", "#ffdf6b", "#ff8484"];

function generateColors(dataLength: number) {
	const colors = [...BASE_COLORS];
	while (colors.length < dataLength) {
		colors.push(`#${Math.floor(Math.random() * 16777215).toString(16)}`);
	}
	return colors;
}

export default function Component({ data }: { data: PieChartItem | PieChartItem[] }) {
	const [activeIndex, setActiveIndex] = useState<number | null>(null);
	const appStore = useAppStore((state) => state);

	const dataArr = useMemo(() => (Array.isArray(data) ? data : [data]), [data]);
	const total = useMemo(() => dataArr?.reduce((acc, entry) => acc + entry.value, 0), []);
	const colors = useMemo(() => generateColors(dataArr.length), [dataArr.length]);

	const handleLegendClick = useCallback((index: number) => {
		setActiveIndex((prevIndex) => (prevIndex === index ? null : index));
	}, []);

	const CustomLabel: FC<LabelProps & { viewBox: { cx: number; cy: number } }> = ({ viewBox: { cx, cy }, value }) => {
		const activeData = activeIndex !== null ? dataArr[activeIndex] : null;
		const displayedValue = activeData ? activeData.value : value;

		return (
			<text x={cx} y={cy} textAnchor="middle" dominantBaseline="middle">
				<tspan x={cx} y={cy} dy="-0.5em" className="text-uppercase">
					{appStore.phrases?.["total"]}
				</tspan>
				<tspan x={cx} y={cy} dy="0.5em" className="fw-bold fs-3">
					{displayedValue}
				</tspan>
			</text>
		);
	};

	return (
		<ResponsiveContainer width="100%" height="100%" minHeight={400} className="pie-chart">
			<PieChart>
				<Pie
					data={dataArr}
					innerRadius={80}
					outerRadius={120}
					dataKey="value"
					cx="50%"
					cy="50%"
					label={({ name }) => name}
				>
					{dataArr?.map((_, index) => (
						<Cell
							key={`cell-${index}`}
							fill={colors[index]}
							fillOpacity={activeIndex === null || activeIndex === index ? 1 : 0.3}
						/>
					))}
					<Label value={total} position="center" content={CustomLabel} />
				</Pie>
				<Tooltip />
				<Legend
					onClick={(e, index) => handleLegendClick(index)}
					formatter={(value, entry, index) => (
						<span
							className="text-black"
							style={{ cursor: "pointer", opacity: activeIndex === null || activeIndex === index ? 1 : 0.3 }}
						>
							{value}
						</span>
					)}
				/>
			</PieChart>
		</ResponsiveContainer>
	);
}
