import "./index.scss";

import React, { useState } from "react";

import { useEffectOnce } from "../../hooks";
import { useAppStore } from "../../stores";
import { http } from "../../utils/client";

export default function Component() {
	const [items, setItems] = useState<{ temp: number; icon: string } | null>(null);

	const appStore = useAppStore((state) => state);

	useEffectOnce(async () => {
		if (appStore.sitePath == null || appStore.appName == null) return;

		try {
			const params = new URLSearchParams();

			params.set("lat", "42.86800164247885");
			params.set("lon", "74.57970010391024");

			const response = await http<typeof items>({
				url: `${appStore.sitePath?.[0].link}/_/service/${appStore.appName}/weather?${params.toString()}`,
			});

			setItems(response.jsonData);
		} catch (e) {
			console.error(e);
		}
	}, [appStore.sitePath, appStore.appName]);

	return (
		<div className="weather">
			{items && (
				<div className="content">
					<img src={items.icon} alt="weather-icon" loading="lazy" />
					<span>{items.temp}℃</span>
				</div>
			)}
		</div>
	);
}
