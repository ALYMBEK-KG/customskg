import "./index.scss";

import { GeoJsonObject } from "geojson";
import { Map, divIcon, geoJson, map, marker, tileLayer } from "leaflet";
import React from "react";

import { useEffectOnce } from "../../hooks";

export default function Component({
	mapId,
	markers,
	polygons,
	onInit,
}: {
	mapId: string;
	markers?: { coordinates: [number, number]; popup?: string }[];
	polygons?: {
		countryCode: string;
		geoJson?: GeoJsonObject;
		popup?: string;
	}[];
	onInit?: (map: Map) => void;
}) {
	const defaultIcon = divIcon({
		className: "marker-primary",
		iconSize: [24, 32],
		iconAnchor: [12, 32],
		html: '<i class="fa-solid fa-location-dot"></i>',
	});

	useEffectOnce(() => {
		const initMap = map(mapId, {
			attributionControl: false,
			scrollWheelZoom: false,
			center: [41.205528, 74.779888],
			zoom: 6,
			minZoom: 4,
			maxZoom: 18,
			maxBounds: [
				[-90, -180],
				[90, 180],
			],
			layers: [tileLayer("https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png")],
		});

		if (polygons) {
			polygons.map((p) => {
				if (p.geoJson == null) return;

				const geoJsonInstance = geoJson(p.geoJson, {
					style: {
						className: "polygon-primary",
						color: "currentColor",
						fillColor: "currentColor",
					},
					onEachFeature: (feature, layer) => {
						layer.on({
							popupopen: (e) => {
								const el = e.target.getElement();
								el.classList.add("active");
							},
							popupclose: (e) => {
								const el = e.target.getElement();
								el.classList.remove("active");
							},
						});
					},
				});

				if (p?.popup) geoJsonInstance.bindPopup(p.popup);

				geoJsonInstance.addTo(initMap);
			});
		}

		if (markers) {
			markers.map((m) => {
				if (m.coordinates == null) return;

				const markerInstance = marker(m.coordinates, {
					icon: defaultIcon,
				});

				if (m?.popup) markerInstance.bindPopup(m.popup);

				markerInstance.addTo(initMap);
			});
		}

		if (onInit != null) onInit(initMap);
	});

	return <div className="map-container" id={mapId}></div>;
}
