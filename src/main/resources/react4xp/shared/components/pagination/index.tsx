import "./index.scss";

import React, { MouseEvent, useEffect, useState } from "react";

export default function Component({
	total,
	currentPage,
	onPageChange,
}: {
	total: number;
	currentPage?: number;
	onPageChange?: (page: number) => void;
}) {
	const dots = 0;
	const siblingCount = 1;
	const pageSize = 12;
	const pageTotal = total > pageSize ? Math.ceil(total / pageSize) : 1;

	const [queryParams, setQueryParams] = useState<URLSearchParams | null>(null);
	const [page, setPage] = useState(currentPage ?? 1);

	const getQueryParams = (key?: string, value?: string) => {
		if (queryParams != null && key != null && value != null) {
			queryParams.set(key, value);
		}

		return queryParams?.toString() ?? "";
	};

	const range = (start: number, end: number) => {
		const length = end - start + 1;

		return Array.from({ length }, (_, idx) => idx + start);
	};

	const getRange = () => {
		const siblingCountTotal = siblingCount * 2 + 3;
		const leftSiblingIndex = Math.max(page - siblingCount, 1);
		const rightSiblingIndex = Math.min(page + siblingCount, pageTotal);
		const shouldShowLeftDots = leftSiblingIndex > 2;
		const shouldShowRightDots = rightSiblingIndex < pageTotal - 2;

		let result = [];

		if (siblingCountTotal >= pageTotal) {
			result = range(1, pageTotal);
		}

		if (!shouldShowLeftDots && shouldShowRightDots) {
			const leftRange = range(1, siblingCountTotal);
			result = [...leftRange, dots, pageTotal];
		}

		if (shouldShowLeftDots && !shouldShowRightDots) {
			const rightRange = range(pageTotal - siblingCountTotal + 1, pageTotal);
			result = [1, dots, ...rightRange];
		}

		if (shouldShowLeftDots && shouldShowRightDots) {
			const middleRange = range(leftSiblingIndex, rightSiblingIndex);
			result = [1, dots, ...middleRange, dots, pageTotal];
		}

		return result;
	};

	const handlePageChange = (e: MouseEvent<HTMLAnchorElement>, page: number) => {
		e.preventDefault();
		setPage(page);

		if (onPageChange != null) {
			onPageChange(page);
		} else {
			location.href = `?${getQueryParams("page", `${page}`)}`;
		}
	};

	useEffect(() => {
		const params = new URLSearchParams(location.search);
		setQueryParams(params);

		const pageParam = params.get("page");
		if (pageParam) setPage(!isNaN(parseInt(pageParam)) ? parseInt(pageParam) : 1);
	}, []);

	return (
		<nav className="my-4">
			<ul className="pagination">
				<li className={`page-item ${page <= 1 ? "disabled" : ""}`}>
					<a
						className="page-link"
						href={`?${getQueryParams("page", `${page - 1}`)}`}
						onClick={(e) => handlePageChange(e, page - 1)}
					>
						<i className="fa-solid fa-arrow-left-long"></i>
					</a>
				</li>

				{getRange().map((item, i) => (
					<li key={`global-search-result-pagination_${i}`} className={`page-item ${item === page ? "active" : ""}`}>
						{item === dots ? (
							<a className="page-link text-muted">
								<i className="fa-solid fa-ellipsis"></i>
							</a>
						) : (
							<a
								className="page-link"
								{...(item !== page
									? { href: `?${getQueryParams("page", `${item}`)}`, onClick: (e) => handlePageChange(e, item) }
									: null)}
							>
								{item}
							</a>
						)}
					</li>
				))}

				<li className={`page-item ${page >= pageTotal ? "disabled" : ""}`}>
					<a
						className="page-link"
						href={`?${getQueryParams("page", `${page + 1}`)}`}
						onClick={(e) => handlePageChange(e, page + 1)}
					>
						<i className="fa-solid fa-arrow-right-long"></i>
					</a>
				</li>
			</ul>
		</nav>
	);
}
