import "./index.scss";

import React, { MouseEvent, useEffect, useState } from "react";

import { useEvent } from "../../../hooks";
import { useAppStore } from "../../../stores";
import type { AppData } from "../../../types/app-data.d";

import Search from "../../../forms/search";
import Currencies from "../../currencies";
import Weather from "../../weather";

enum Flags {
	en = "gb",
	ru = "ru",
	ky = "kg",
}

export default function Component() {
	const appStore = useAppStore((state) => state);

	const [menuOpen, setMenuOpen] = useState(false);
	const [navMenuList, setNavMenuList] = useState<Record<string, AppData["navMenuList"]["hits"]> | null>(null);

	const handleMenuToggle = (e: MouseEvent<HTMLButtonElement>) => {
		e.stopPropagation();
		setMenuOpen((prev) => !prev);
	};

	const handleMenuContentClick = (e: MouseEvent<HTMLUListElement>) => {
		e.stopPropagation();
	};

	useEvent(() => {
		setMenuOpen(false);
	});

	useEffect(() => {
		if (appStore.navMenuList?.hits.length > 0) {
			setNavMenuList(
				appStore.navMenuList.hits
					.filter((item) => item.data.groups?.includes("header"))
					.reduce((acc = {}, item) => {
						const key = item.data.category;
						acc[key] ??= [];
						acc[key].push(item);
						return acc;
					}, {})
			);
		}
	}, [appStore.navMenuList]);

	return (
		<div className="header-bottom">
			<div className={`overlay ${appStore.isHeaderTransparent ? "transparent" : ""}`}>
				<div className={`menu-content burger-menu ${menuOpen ? "open" : ""}`}>
					<div className="container">
						<div className="row">
							<div className="col-12">
								<nav className="nav rounded nav-menu" onClick={handleMenuContentClick}>
									{navMenuList &&
										Object.entries(navMenuList).map(([category, items]) => (
											<div key={category} className={`nav-block ${category == "undefined" ? "full" : ""}`}>
												{category != "undefined" && <h6 className="mb-1">{category}</h6>}
												{items.map(
													({ _id, displayName, data: { link, contentTypeImage, contentTypeImageAltText } }) => (
														<a
															key={_id}
															className={`nav-link nav-link-text ${appStore.currentPage === link ? "active" : ""}`}
															href={link}
														>
															{contentTypeImage && (
																<img
																	src={contentTypeImage}
																	alt={contentTypeImageAltText}
																	loading="lazy"
																	className="icon-image"
																/>
															)}
															{displayName}
														</a>
													)
												)}
											</div>
										))}

									{(navMenuList == null || Object.keys(navMenuList).length === 0) && (
										<div className="empty">
											<h3>{appStore.phrases?.["empty"]}</h3>
										</div>
									)}
								</nav>
							</div>
						</div>
					</div>
				</div>
				<div className="container">
					<div className="row align-items-center">
						<div className="col-2 col-md-6 d-flex">
							<button className="btn menu-toggler" onClick={handleMenuToggle}>
								<i className="fa-solid fa-bars"></i>
							</button>

							<div className="d-none d-md-inline-block w-100">
								<Search />
							</div>
						</div>

						<div className="col-10 col-md-6 d-flex justify-content-end">
							<Currencies />

							<Weather />

							<div className="dropdown">
								<button type="button" className="btn dropdown-toggle text-uppercase" data-bs-toggle="dropdown">
									<i className={`fi fi-${Flags[appStore.locale]}`}></i>
									{appStore.locale}
								</button>
								<ul className="dropdown-menu dropdown-menu-end shadow border-0">
									{appStore.locales?.map((item) => (
										<li key={`locale-${item.locale}`}>
											<a
												className={`dropdown-item text-uppercase d-flex gap-1 ${
													item.locale === appStore.locale ? "active" : ""
												}`}
												href={item.link}
											>
												<i className={`fi fi-${Flags[item.locale]}`}></i>
												<span>{item.locale}</span>
											</a>
										</li>
									))}
								</ul>
							</div>
						</div>
					</div>

					<div className="row align-items-center">
						<div className="col-12">
							<div className="d-md-none">
								<Search />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
