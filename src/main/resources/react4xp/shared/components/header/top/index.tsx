import "./index.scss";

import React from "react";

import { useEffectOnce } from "../../../hooks";
import { useAppStore } from "../../../stores";
import { http } from "../../../utils/client";

import SignIn from "../../../forms/user/sign-in";
import SignUp from "../../../forms/user/sign-up";

export default function Component() {
	const appStore = useAppStore((state) => state);
	const appStoreUpdate = useAppStore((state) => state.update);
	const appStoreAlert = useAppStore((state) => state.alert);

	const handleRedirect = () => {
		location.href = appStore.siteData?.externalCabinetLink;
	};

	const handleSignOut = async () => {
		if (appStore.sitePath == null || appStore.appName == null || appStore.authData == null) return;

		try {
			const result = await http({
				url: `${appStore.sitePath[0].link}/_/service/${appStore.appName}/sign-out`,
				headers: { Accept: "text/plain", Cookie: appStore.authData.cookie },
			});

			if (result.ok) {
				appStoreUpdate({ authData: null });
			}
		} catch (e) {
			appStoreAlert({ type: "danger", title: appStore.phrases?.["anUnexpectedErrorOccurred"] });
		}
	};

	useEffectOnce(async () => {
		if (appStore.sitePath == null || appStore.appName == null || appStore.authData == null) return;

		try {
			const params = new URLSearchParams();

			params.set("id", `${appStore.authData.user.id}`);
			params.set("cookie", `${appStore.authData.cookie}`);

			const userResponse = await http({
				url: `${appStore.sitePath[0].link}/_/service/${appStore.appName}/user?${params.toString()}`,
			});

			if (userResponse.status === 401) throw new Error(`${userResponse.status} ${userResponse.statusText}`);
		} catch (e) {
			appStoreUpdate({ authData: null });
		}
	}, [appStore.sitePath, appStore.appName, appStore.authData, appStoreUpdate]);

	return (
		<div className="header-top">
			<div className="container">
				<div className="row align-items-center">
					<div className="col-12 col-md-6">
						<a className="logo" href={appStore.sitePath?.[0].link ?? ""}>
							<img src={appStore.siteData?.logo} alt={appStore.phrases?.["logo"]} loading="lazy" />
							<div>
								<h5>{appStore.siteData?.logoTitle}</h5>
								<p>{appStore.siteData?.logoSubtitle}</p>
							</div>
						</a>
					</div>

					<div className="col-12 col-md-6 text-end d-flex justify-content-start justify-content-md-end align-items-center mt-md-0 mt-2">
						<div className="d-flex gap-2 me-4">
							<a href={appStore.siteData?.instagram} className="fs-3">
								<i className="fa-brands fa-instagram"></i>
							</a>
							<a href={appStore.siteData?.facebook} className="fs-3">
								<i className="fa-brands fa-facebook"></i>
							</a>
						</div>
						<div className="dropdown">
							<button
								type="button"
								className="btn btn-primary btn-primary-light"
								data-bs-auto-close="outside"
								onClick={handleRedirect}
							>
								{appStore.authData?.user?.fullName ?? appStore.phrases?.["cabinet"]}
								<i className="fa-solid fa-user"></i>
							</button>

							<div className="dropdown-menu dropdown-menu-end shadow border-0 p-0">
								{appStore.authData?.user == null ? (
									<div className="forms">
										<div className="nav nav-tabs" id="cabinet-tab">
											<button
												type="button"
												className="btn nav-link flex-grow-1 p-3 active"
												id="cabinet-signin-tab"
												data-bs-toggle="tab"
												data-bs-target="#cabinet-signin-content"
											>
												{appStore.phrases?.["signIn"]}
												<i className="fa-solid fa-right-to-bracket"></i>
											</button>

											<button
												type="button"
												className="btn nav-link flex-grow-1 p-3"
												id="cabinet-signup-tab"
												data-bs-toggle="tab"
												data-bs-target="#cabinet-signup-content"
											>
												{appStore.phrases?.["signUp"]}
												<i className="fa-solid fa-user-plus"></i>
											</button>
										</div>

										<div className="tab-content w-100 p-3" id="cabinet-tabContent">
											<div className="tab-pane fade w-100 h-100 show active" id="cabinet-signin-content">
												<SignIn />
											</div>

											<div className="tab-pane fade w-100 h-100" id="cabinet-signup-content">
												<SignUp />
											</div>
										</div>
									</div>
								) : (
									<div className="actions">
										<a className="btn justify-content-start" onClick={handleRedirect}>
											<i className="fa-solid fa-right-to-bracket"></i>
											<span>{appStore.phrases?.["cabinet"]}</span>
										</a>
										<hr className="m-0" />
										<a className="btn justify-content-start" onClick={handleSignOut}>
											<i className="fa-solid fa-right-from-bracket"></i>
											<span>{appStore.phrases?.["signOut"]}</span>
										</a>
									</div>
								)}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
