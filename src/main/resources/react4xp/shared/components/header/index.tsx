import "./index.scss";

import React from "react";
import HeaderBottom from "./bottom";
import HeaderTop from "./top";

export default function Component() {
	return (
		<header className="header">
			<HeaderTop />
			<HeaderBottom />
		</header>
	);
}
