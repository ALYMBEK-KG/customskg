import "./index.scss";

import React from "react";

import { useAppStore } from "../../stores";

export default function Component() {
	const appStore = useAppStore((state) => state);

	return (
		<div className="container">
			<div className="row">
				<div className="col">
					{appStore.sitePath?.length > 1 && (
						<nav className="my-3">
							<ol className="breadcrumb">
								{appStore.sitePath?.map((item, i) =>
									i === appStore.sitePath.length - 1 ? (
										<li key={`breadcrumb-${i}`} className="breadcrumb-item active">
											{item.title}
										</li>
									) : (
										<li key={`breadcrumb-${i}`} className="breadcrumb-item">
											<a href={item.link}>
												{i === 0 ? (
													<i className="fa-solid fa-house" title={appStore.phrases?.["home"]}></i>
												) : (
													item.title
												)}
											</a>
										</li>
									)
								)}
							</ol>
						</nav>
					)}
				</div>
			</div>
		</div>
	);
}
