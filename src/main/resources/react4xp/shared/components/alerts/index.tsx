import "./index.scss";

import React from "react";

import { useEffectOnce } from "../../hooks";
import { useAppStore } from "../../stores";

export default function Component({
	alignment = "bottomRight",
	timer = 10000,
}: {
	alignment?: "topLeft" | "topRight" | "bottomLeft" | "bottomRight";
	timer?: number;
}) {
	let alignmentClasses = "";

	switch (alignment) {
		case "topLeft":
			alignmentClasses = "top-0 start-0";
			break;
		case "topRight":
			alignmentClasses = "top-0 end-0";
			break;
		case "bottomLeft":
			alignmentClasses = "bottom-0 start-0";
			break;
		case "bottomRight":
		default:
			alignmentClasses = "bottom-0 end-0";
	}

	const appStore = useAppStore((state) => state);
	const appStoreUpdate = useAppStore((state) => state.update);

	useEffectOnce(
		() => {
			if (appStore.alerts != null) appStoreUpdate({ alerts: null });
		},
		[timer, appStore, appStoreUpdate],
		timer
	);

	return (
		<div className={`toast-container position-fixed ${alignmentClasses}`}>
			{appStore.alerts?.map(({ type, title, link }, i) => (
				<div
					key={`alert-${i}`}
					className={`toast align-items-center border-0 text-white show z-2 bg-${type ?? "info"}`}
				>
					<div className="d-flex align-items-center justify-content-between gap-2">
						<div className="toast-body">
							<a className="btn text-white p-1" {...(link ? { href: link } : {})}>
								{title}
							</a>
						</div>
						<button type="button" className="btn-close btn-close-white p-3" data-bs-dismiss="toast"></button>
					</div>
				</div>
			))}
		</div>
	);
}
