import React from "react";
import { ProgressBarProps } from "../../helper";

export default function Component({ value }: ProgressBarProps) {
	return (
		<div
			className="progress w-100"
			role="progressbar"
			aria-label="Basic example"
			aria-valuenow={value}
			aria-valuemin={0}
			aria-valuemax={100}
		>
			<div className="progress-bar" style={{ width: `${value}%` }}>
				{value}%
			</div>
		</div>
	);
}
