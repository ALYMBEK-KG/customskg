export const ratingLabels = ["soso", "satisfactory", "average", "good", "excellent"];

export interface RatingResponse {
	status: number;
	jsonData?: {
		status: number;
		data:
			| boolean
			| {
					value1: number;
					value2: number;
					value3: number;
					value4: number;
					value5: number;
			  };
	};
}

export interface ProgressBarProps {
	value: number;
}
