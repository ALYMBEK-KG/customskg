import React, { useCallback, useState } from "react";

import { useEffectOnce } from "../../hooks";
import { useAppStore } from "../../stores";
import { http } from "../../utils/client";
import ProgressBar from "./elements/progress-bar";
import { ratingLabels, RatingResponse } from "./helper";
import "./index.scss";

export default function Component() {
	const [rating, setRating] = useState(0);
	const [ratingData, setRatingData] = useState([]);
	const [isModalOpen, setIsModalOpen] = useState(false);
	const [isVoted, setIsVoted] = useState(false);
	const appStore = useAppStore((state) => state);
	const appStoreAlert = useAppStore((state) => state.alert);
	const checkVote = async () => {
		try {
			const response = await http({
				url: `${appStore.sitePath?.[0].link}/_/service/${appStore.appName}/rate`,
				method: "GET",
			});

			const jsonData = response.jsonData as Record<string, unknown>;

			if (!jsonData.data) {
				return false;
			} else {
				const ratingArrayData = Object.values(jsonData.data);
				setRatingData(ratingArrayData);
				return true;
			}
		} catch (error) {
			appStoreAlert({ type: "danger", title: appStore.phrases?.["anUnexpectedErrorOccurred"] });
		}
	};

	useEffectOnce(async () => {
		const hasVoted = await checkVote();
		setIsVoted(hasVoted);
	}, [appStore.sitePath, appStore.appName, appStore.siteData]);

	const handleRatingSubmit = useCallback(
		async (currentRate: number) => {
			setRating(currentRate);

			try {
				const url = `${appStore.sitePath?.[0].link}/_/service/${appStore.appName}/rate`;
				const res: RatingResponse = await http({
					url: url,
					method: "POST",
					body: JSON.stringify(currentRate ?? "null"),
				});

				const ratingArrayData = Object.values(res.jsonData.data);
				setRatingData(ratingArrayData);
				setIsVoted(true);
			} catch (error) {
				appStoreAlert({ type: "danger", title: appStore.phrases?.["anUnexpectedErrorOccurred"] });
			}
		},
		[appStore]
	);
	return (
		<>
			<div className="rating_container">
				{!isModalOpen && (
					<button onClick={() => setIsModalOpen(true)} className="rate_open_btn">
						<span
							data-toggle="tooltip"
							data-placement="top"
							title={appStore.phrases?.["customsPerformanceAssessment"]}
							className="rate_btn"
						>
							<img src={appStore.imageLinks?.feedbackIcon} alt="" className="feedback_icon" />
						</span>
					</button>
				)}
				{isModalOpen && (
					<div className="d-flex flex-column justify-content-start rounded p-0 text-white bg-white overflow-hidden rate_content">
						<div className="bg-primary d-flex justify-content-between align-items-center p-3 gap-4">
							<p className="fw-semibold m-0">{appStore.phrases?.["assessThePerformanceOfTheCustomsService"]}</p>
							<i className="fa-solid fa-x" onClick={() => setIsModalOpen(false)} style={{ cursor: "pointer" }}></i>
						</div>

						<div className="p-4">
							{!isVoted && (
								<div>
									<div className="d-flex justify-content-center align-items-center rounded-2 bg-white overflow-hidden border border-primary">
										{ratingLabels.map((label, index) => {
											const currentRate = index + 1;
											return (
												<>
													<label
														key={index}
														className="w-100 text-center"
														data-toggle="tooltip"
														data-placement="top"
														title={appStore.phrases?.[label]}
													>
														<input
															type="radio"
															name="rate"
															value={currentRate}
															onClick={() => handleRatingSubmit(currentRate)}
															style={{ display: "none" }}
														/>
														<i
															className={`d-block py-3 px-4 border ${
																currentRate <= rating ? "border-white" : "border-primary"
															} ${currentRate === 1 ? "rounded-start-2" : ""}  ${
																currentRate === 5 ? "rounded-end-2" : ""
															} item`}
															style={{
																backgroundColor: currentRate <= rating ? "#355cb7" : "#fff",
																color: currentRate <= rating ? "#fff" : "#355cb7",
																cursor: "pointer",
																fontStyle: "normal",
															}}
														>
															{currentRate}
														</i>
													</label>
												</>
											);
										})}
									</div>
									<div className="d-flex align-items-center justify-content-between text-uppercase text-secondary py-2">
										<p className="mb-0">{appStore.phrases?.["soso"]}</p>
										<p className="mb-0">{appStore.phrases?.["excellent"]}</p>
									</div>
								</div>
							)}
							{isVoted && (
								<div className="d-flex flex-column gap-1">
									{ratingData.length !== 0 ? (
										ratingData.map((value: number, idx: number) => (
											<div className="d-flex align-items-center gap-3 text-black" key={idx}>
												<span>{idx + 1}</span>
												<ProgressBar value={value} />
											</div>
										))
									) : (
										<div className="spinner-border text-primary m-auto"></div>
									)}
								</div>
							)}
						</div>
					</div>
				)}
			</div>
		</>
	);
}
