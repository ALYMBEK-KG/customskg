import "./index.scss";

import React, { useState } from "react";

import { useEffectOnce } from "../../hooks";
import { useAppStore } from "../../stores";
import { http } from "../../utils/client";

export default function Component() {
	const [items, setItems] = useState<{ code: string; value: number }[]>([]);

	const appStore = useAppStore((state) => state);
	const appStoreAlert = useAppStore((state) => state.alert);

	useEffectOnce(async () => {
		if (appStore.sitePath == null || appStore.appName == null || appStore.siteData?.currencies == null) return;

		try {
			const response = await http<typeof items>({
				url: `${appStore.sitePath?.[0].link}/_/service/${appStore.appName}/currency?code=${appStore.siteData.currencies}`,
			});

			setItems(response.jsonData);
		} catch (e) {
			appStoreAlert({ type: "danger", title: appStore.phrases?.["anUnexpectedErrorOccurred"] });
		}
	}, [appStore.sitePath, appStore.appName, appStore.siteData]);

	return (
		<div id="currencies-carousel" className="carousel slide" data-bs-ride="carousel">
			{items?.length > 0 && (
				<div className="carousel-inner">
					{items?.map(({ code, value }, i) => (
						<div key={`slide-${i}`} className={`carousel-item ${i === 0 ? "active" : ""}`}>
							<div className="fw-bold">{code}</div>
							<div>{value}</div>
						</div>
					))}
				</div>
			)}
		</div>
	);
}
