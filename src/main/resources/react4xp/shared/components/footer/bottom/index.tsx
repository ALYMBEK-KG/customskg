import "./index.scss";

import React from "react";

import { useAppStore } from "../../../stores";
import { YANDEX_METRIKA_ID } from "/react4xp/shared/utils/server/constants";

export default function Component() {
	const appStore = useAppStore((state) => state);

	return (
		<div className="footer-bottom">
			<div className="container">
				<div className="row align-items-center">
					<div className="col-12 col-md-6">
						<a className="logo" href={appStore.sitePath?.[0].link ?? ""}>
							<img src={appStore.siteData?.logo} alt={appStore.phrases?.["logo"]} loading="lazy" />
							<div>
								<h5>{appStore.siteData?.logoTitle}</h5>
								<p>{appStore.siteData?.logoSubtitle}</p>
							</div>
						</a>
					</div>

					<div className="col-12 col-md-6 text-end">
						<a
							href={`https://metrika.yandex.ru/stat/?id=${YANDEX_METRIKA_ID}&amp;from=informer`}
							target="_blank"
							rel="nofollow noreferrer"
						>
							<img
								src={`https://informer.yandex.ru/informer/${YANDEX_METRIKA_ID}/3_1_F0FFFFFF_D0E5FBFF_0_pageviews`}
								style={{ width: 88, height: 31, border: 0 }}
								alt="Яндекс.Метрика"
								title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
								className="ym-advanced-informer"
								data-cid={YANDEX_METRIKA_ID}
								data-lang="ru"
							/>
						</a>
					</div>
				</div>
			</div>
		</div>
	);
}
