import "./index.scss";

import React, { useEffect, useState } from "react";

import { useAppStore } from "../../../stores";
import type { AppData } from "../../../types/app-data.d";

export default function Component() {
	const appStore = useAppStore((state) => state);

	const [navMenuList, setNavMenuList] = useState<Record<string, AppData["navMenuList"]["hits"]> | null>(null);

	useEffect(() => {
		if (appStore.navMenuList?.hits.length > 0) {
			setNavMenuList(
				appStore.navMenuList.hits
					.filter((item) => item.data.groups?.includes("footer"))
					.reduce((acc = {}, item) => {
						const key = item.data.category;
						acc[key] ??= [];
						acc[key].push(item);
						return acc;
					}, {})
			);
		}
	}, [appStore.navMenuList]);

	return (
		<div className="footer-top">
			<div className="container">
				<div className="row">
					<div className="col-12 col-md-9">
						<nav className="nav">
							{navMenuList &&
								Object.entries(navMenuList).map(([category, items]) => (
									<div key={category} className={`nav-block ${category == "undefined" ? "full" : ""}`}>
										{category != "undefined" && <h6 className="mb-1">{category}</h6>}
										{items.map(({ _id, displayName, data: { link, contentTypeImage, contentTypeImageAltText } }) => (
											<a key={_id} className={`nav-link ${appStore.currentPage === link ? "active" : ""}`} href={link}>
												{contentTypeImage && (
													<img src={contentTypeImage} alt={contentTypeImageAltText} loading="lazy" />
												)}
												{displayName}
											</a>
										))}
									</div>
								))}
						</nav>
					</div>

					<div className="col-12 col-md-3">
						<div className="mt-3 mt-md-0 mb-3">
							<div className="mb-1">
								<i className="fa-solid fa-phone text-primary me-2"></i>
								<span className="fw-bold">{appStore.phrases?.["phone"]}:</span>
							</div>
							<a href={`tel:${appStore.siteData?.phone}`}>{appStore.siteData?.phone}</a>
						</div>
						<div className="mt-3 mt-md-0 mb-3">
							<div className="mb-1">
								<i className="fa-solid fa-envelope text-primary me-2"></i>
								<span className="fw-bold">{appStore.phrases?.["email"]}:</span>
							</div>
							<a href={`mailto:${appStore.siteData?.email}`}>{appStore.siteData?.email}</a>
						</div>
						<div className="mt-3 mt-md-0 mb-3">
							<div className="mb-1">
								<i className="fa-solid fa-location-dot text-primary me-2"></i>
								<span className="fw-bold">{appStore.phrases?.["address"]}:</span>
							</div>
							<span>{appStore.siteData?.address}</span>
						</div>
						<div className="mt-3 mt-md-0 mb-3">
							<div className="mb-1">
								<i className="fa-solid fa-headphones text-primary me-2"></i>
								<span className="fw-bold">{appStore.phrases?.["helpline"]}:</span>
							</div>
							<a href={`tel:${appStore.siteData?.helpline}`}>{appStore.siteData?.helpline}</a>
						</div>
						<div className="mt-3 mt-md-0 mb-3">
							<div className="mb-1">
								<i className="fa-solid fa-arrow-up-right-from-square text-primary me-2"></i>
								<a href={appStore.siteData?.oldSite}>{appStore.phrases?.["oldSite"]}</a>
							</div>
						</div>
						<div className="d-flex gap-2 mt-3 mt-md-0 mb-3">
							<a href={appStore.siteData?.instagram} className="fs-2">
								<i className="fa-brands fa-instagram"></i>
							</a>
							<a href={appStore.siteData?.facebook} className="fs-2">
								<i className="fa-brands fa-facebook"></i>
							</a>
							<a href={appStore.siteData?.whatsapp} className="fs-2">
								<i className="fa-brands fa-whatsapp"></i>
							</a>
							<a href={appStore.siteData?.telegram} className="fs-2">
								<i className="fa-brands fa-telegram"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
