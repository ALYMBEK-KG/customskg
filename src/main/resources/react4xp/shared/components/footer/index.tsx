import "./index.scss";

import React from "react";

import FooterBottom from "./bottom";
import FooterTop from "./top";

import { useAppStore } from "../../stores";

export default function Component() {
	const appStore = useAppStore((state) => state);

	return (
		<footer
			className="footer"
			style={{
				backgroundImage: `url("${appStore.imageLinks?.kgOrnament ?? ""}")`,
			}}
		>
			<FooterTop />
			<FooterBottom />
		</footer>
	);
}
