import html2canvas from "html2canvas";
import { PDFDocument } from "pdf-lib";
import React, { ReactNode, RefObject, useRef } from "react";

interface PDFRendererProps {
	fileName?: string;
	slots?: {
		header?: (actions: { download: () => void; print: () => void }) => ReactNode;
		body: (ref: RefObject<HTMLDivElement>) => ReactNode;
	};
}

export default function Component({ slots, fileName }: PDFRendererProps) {
	const bodyRef = useRef<HTMLDivElement>(null);

	const download = async () => {
		const element = bodyRef.current;

		const canvas = await html2canvas(element);
		const imgData = canvas.toDataURL("image/png");

		const pdfDoc = await PDFDocument.create();
		const page = pdfDoc.addPage([canvas.width, canvas.height]);

		const pngImage = await pdfDoc.embedPng(imgData);
		const { width, height } = page.getSize();
		page.drawImage(pngImage, {
			x: 0,
			y: 0,
			width: width,
			height: height,
		});

		const pdfBytes = await pdfDoc.save();

		const link = document.createElement("a");
		link.href = URL.createObjectURL(new Blob([pdfBytes], { type: "application/pdf" }));
		link.download = `${fileName}.pdf`;

		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	};

	const print = async () => {
		const element = bodyRef.current;
		const canvas = await html2canvas(element);
		const imgData = canvas.toDataURL("image/png");
		const printWindow = window.open("", "_blank");
		if (printWindow) {
			printWindow.document.write(
				`<html><head><title>${fileName}</title></head><body><img src="${imgData}"></body></html>`
			);
			printWindow.document.close();
			printWindow.onload = () => {
				printWindow.print();
				printWindow.close();
			};
		}
	};

	return (
		<div className="d-flex flex-column w-100">
			{slots?.header ? (
				slots.header({ download, print })
			) : (
				<div className="d-flex justify-content-end align-items-center p-3 gap-2">
					<button className="btn btn-primary" onClick={download}>
						<i className="fa-solid fa-file-arrow-down"></i>
					</button>
					<button className="btn btn-primary" onClick={print}>
						<i className="fa-solid fa-print"></i>
					</button>
				</div>
			)}
			{slots?.body && slots.body(bodyRef)}
		</div>
	);
}
