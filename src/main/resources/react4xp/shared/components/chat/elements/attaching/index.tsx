import React from "react";
export default function Component({
	setMessageFileUrl,
	setFile,
}: {
	setMessageFileUrl: (file: string | ArrayBuffer) => void;
	setFile: (file: File) => void;
}) {
	const onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		e.stopPropagation();
		const file: File = e.target.files[0];
		e.target.value = null;
		if (file) {
			const reader: FileReader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function (e) {
				const messageFileUrl: string | ArrayBuffer = e.target.result;
				setFile(file);
				setMessageFileUrl(messageFileUrl);
			};
		}
	};
	return (
		<div className="addFile">
			<div>
				<i className="fa-solid fa-paperclip attachIcon"></i>
			</div>
			<input
				onChange={onChangeInput}
				type="file"
				className="inputFile"
				accept="image/jpeg, image/png, text/plain, application/pdf, application/vnd.ms-powerpoint, application/msword, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,image/webp"
			/>
		</div>
	);
}
