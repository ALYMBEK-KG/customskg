import React, { useState } from "react";
import FormChat from "../../elements/form-chat";

export default function Component({
	setShowIcons,
	setClassNameWidget,
}: {
	setShowIcons: (value: boolean) => void;
	setClassNameWidget: (value: string) => void;
}) {
	const [formStyleBottom, setFormStyleBottom] = useState<string>("-1000px");
	const onClickClose = () => {
		setShowIcons(false);
		setClassNameWidget("widget_container");
	};
	const onClickChatIcon = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
		e.preventDefault();
		setFormStyleBottom("5px");
	};
	return (
		<>
			<a className="btn_messenger btn_whatsapp" href="https://wa.me/996500910850">
				<span className="btn_whatsapp">
					<i className="fa-brands fa-whatsapp"></i>
				</span>
			</a>
			<a className="btn_messenger" href="tel:1240">
				<span className="btn_call">
					<i className="fa-solid fa-phone-volume"></i>
				</span>
			</a>
			<a className="btn_messenger" style={{ position: "relative" }} onClick={onClickChatIcon}>
				<span className="btn_onlinechat">
					<i className="fa-regular fa-comments"></i>
				</span>
			</a>
			<button className="btn_messenger btn_close" onClick={onClickClose}>
				<i className="fa-solid fa-xmark"></i>
			</button>
			<FormChat formStyleBottom={formStyleBottom} setFormStyleBottom={setFormStyleBottom} />
		</>
	);
}
