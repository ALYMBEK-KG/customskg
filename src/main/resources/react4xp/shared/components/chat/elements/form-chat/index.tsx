import React, { ChangeEvent, FormEvent, useEffect, useRef, useState } from "react";

import ReCAPTCHA from "react-google-recaptcha";
import {
	MessageType,
	UserType,
	axelorDomain,
	createTimeStamp,
	domain,
	fileTypeService,
	parseTimeStamp,
	token,
} from "../../helpers";
import AttachingFile from "../attaching";
import "./onlineChatBody.scss";
import { useAppStore } from "/react4xp/shared/stores";
import { useChatStore } from "/react4xp/shared/stores/chat";
import { getCookie, http, setCookie } from "/react4xp/shared/utils/client";
import { PUBLIC_RECAPTCHA_KEY } from "/react4xp/shared/utils/server/constants";

interface JsonDataType {
	status: number;
	user: UserType;
}

interface RecaptchaResponse {
	success: boolean;
	challenge_ts: string;
	hostname: string;
}

export default function Component({
	formStyleBottom,
	setFormStyleBottom,
}: {
	formStyleBottom: string;
	setFormStyleBottom: (value: string) => void;
}) {
	const [captchaVerified, setCaptchaVerified] = useState(false);
	const recaptchaRef = useRef<ReCAPTCHA | null>(null);
	const [nameInputClassName, setNameInputClassName] = useState<string>("name-input");
	const [telInputClassName, setTelInputClassName] = useState<string>("tel-input");
	const [messageTextareaClassName, setMessageTextareaClassName] = useState<string>("message-textarea");
	const [isLoading, setIsLoading] = useState<string>("");
	const nameRef = useRef<HTMLInputElement | null>(null);
	const telRef = useRef<HTMLInputElement | null>(null);
	const messageTextareaRef = useRef<HTMLTextAreaElement | null>(null);
	const [btnSendMessageClassName, setBtnSendMessageClassName] = useState<string>("message-button-two");

	const [messageFileUrl, setMessageFileUrl] = useState<string | ArrayBuffer | null>(null);
	const [file, setFile] = useState<File | null>(null);
	const [inputValue, setInputValue] = useState<string>("");
	const chatStore = useChatStore((state) => state);
	const appStore = useAppStore((state) => state);
	useEffect(() => {
		chatStore.SseOn();
	}, []);

	const onChangeTelInput = (e: ChangeEvent<HTMLInputElement>) => {
		const input = e.target.value.replace(/\D/g, "");

		let formatted = "+(996)";

		for (let i = 3; i < input.length && i < 12; i++) {
			if (i === 3 || i === 6 || i === 8 || i === 10) {
				formatted += " ";
			}
			formatted += input[i];
		}

		telRef.current.value = formatted;
	};

	const onFocusNameInpit = () => {
		setNameInputClassName("name-input input-focus-name");
	};

	const onBlurNameInput = () => {
		setNameInputClassName("name-input");
	};

	const onFocusTelInput = () => {
		setTelInputClassName("tel-input input-focus-tel");
	};

	const onBlurTelInput = () => {
		setTelInputClassName("tel-input");
	};

	const onFocusMessageTextarea = () => {
		setMessageTextareaClassName("message-textarea input-focus-email");
	};

	const onBlurMessageTextarea = () => {
		setMessageTextareaClassName("message-textarea");
	};

	const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		const name = nameRef.current.value;
		const tel = telRef.current.value.replace(/[\s+()]/g, "");
		const message = messageTextareaRef.current.value;

		if (name.length < 3) {
			setNameInputClassName("name-input error");
		} else {
			setNameInputClassName("name-input");
		}

		if (tel.length < 12) {
			setTelInputClassName("tel-input error");
		} else {
			setTelInputClassName("tel-input");
		}
		try {
			setIsLoading("pending");

			const response = await http<JsonDataType>({
				url: `${domain}/webhook/onlineChat`,
				method: "POST",
				body: JSON.stringify({
					name,
					number: tel,
					textareaMessage: message,
					timestamp: createTimeStamp(),
				}),
			});
			if (!response.ok) {
				throw new Error("Произошла ошибка с сетью! Повторите попытку позже!");
			}
			const jsonData: JsonDataType = response.jsonData;
			const cookieUser: string = JSON.stringify(jsonData.user);
			setCookie("onlineChatUser", cookieUser, 168, "/");

			chatStore.SseOn();
			setIsLoading("continue");
		} catch (error) {
			setIsLoading("error");
		}
	};

	const onCaptchaChange = async (token: string | null) => {
		const params = new URLSearchParams();
		params.set("token", `${token}`);

		const response = await http({
			url: `${appStore.sitePath?.[0].link}/_/service/${appStore.appName}/recaptcha?${params.toString()}`,
			method: "POST",
		});
		const jsonData: RecaptchaResponse = await response.json();

		if (jsonData.success) {
			setCaptchaVerified(true);
		}
	};

	const onClickCloseForm = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		e.stopPropagation();
		setFormStyleBottom("-1000px");
	};

	const onClickCloseFile = () => {
		setMessageFileUrl(null);
	};
	useEffect(() => {
		console.log({ messages: chatStore.messages });
	}, [chatStore.messages]);

	const onClickSendFile = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		e.stopPropagation();
		const cookieValue: string = getCookie("onlineChatUser");
		const user: UserType = cookieValue ? JSON.parse(cookieValue) : undefined;
		const type: string = fileTypeService(file.type);
		const messages: MessageType[] = chatStore.messages;
		const newMessage: MessageType = {
			body: "",
			clientName: {
				fullName: user?.userName,
				id: user?.id,
			},
			messageAuthor: "client",
			messageSecretKey: null,
			operatorName: null,
			timestamp: createTimeStamp(),
			type: type,
			fileName: file.name,
			fileUrlArray: type === "image" ? messageFileUrl : null,
			status: "pending",
			id: Math.random() * 1000,
			fromNumber: user?.userPhone,
		};
		messages.unshift(newMessage);
		chatStore.setMessages(messages);
		sendFile({ user });
		setMessageFileUrl(null);
	};

	const sendFile = async ({ user }: { user: UserType }) => {
		try {
			const id: string = String(user.id);
			const userPhone: string = user.userPhone;
			const date: Date = new Date();
			const timestamp: number = date.getTime() / 1000;
			const formData: FormData = new FormData();
			formData.append("file", file);
			formData.append("userPhone", userPhone);
			formData.append("id", id);
			formData.append("timestamp", `${timestamp}`);
			formData.append("messageAuthor", "client");
			await fetch(domain + "/webhook/onlineChat/upload", {
				method: "POST",
				headers: {
					Authorization: "Bearer " + token,
				},
				body: formData,
			});
			setFile(null);
		} catch (error) {
			console.log(error);
		}
	};

	const onKeyUpInput = () => {
		if (inputValue.length > 3) {
			setBtnSendMessageClassName("message-button-two message-button-two__active");
		} else {
			setBtnSendMessageClassName("message-button-two");
		}
	};

	const onClickSendMessage = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		e.stopPropagation();
		if (inputValue.length > 3) {
			const cookieValue: string = getCookie("onlineChatUser");
			const user: UserType = cookieValue ? JSON.parse(cookieValue) : undefined;
			const clientName = { fullName: user.userName, id: user.id };
			const message: string = inputValue;
			setInputValue("");
			const timestamp = createTimeStamp();
			const messages = chatStore.messages;
			messages.unshift({
				body: message,
				messageAuthor: "client",
				status: "pending",
				time: parseTimeStamp(timestamp),
				type: "text",
				clientName,
				messageSecretKey: null,
				id: Math.random() * 1000,
				fromNumber: user.userPhone,
				operatorName: null,
				timestamp: timestamp,
			});
			sendMessage({ message, timestamp, user });
		}
	};

	const sendMessage = async ({ message, timestamp, user }: { message: string; timestamp: number; user: UserType }) => {
		const obj = {
			data: {
				userName: user.userName,
				userPhone: user.userPhone,
				id: user.id,
				message: message,
				timestamp,
			},
		};
		try {
			await http({
				url: domain + "/webhook/onlineChat/newMessage",
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + token,
				},
				body: JSON.stringify(obj),
			});
		} catch (error) {
			console.log(error);
		}
	};

	return (
		<form className="chat-form" onSubmit={onSubmit} style={{ bottom: formStyleBottom }}>
			<div className="top-div">
				<h3 className="title">{chatStore.sse ? "Напишите сообщение" : "Представьтесь, пожалуйста"}</h3>
			</div>
			{isLoading === "" && !chatStore.sse && (
				<div className="bottom-div">
					<label htmlFor="" className="name-label">
						Имя <span style={{ color: "red" }}>*</span>
					</label>
					<input
						type="text"
						name="name"
						required
						className={nameInputClassName}
						onFocus={onFocusNameInpit}
						onBlur={onBlurNameInput}
						ref={nameRef}
					/>
					<label htmlFor="" className="tel-label">
						Телефон <span style={{ color: "red" }}>*</span>
					</label>
					<input
						type="tel"
						className={telInputClassName}
						name="tel"
						required
						onChange={onChangeTelInput}
						onFocus={onFocusTelInput}
						onBlur={onBlurTelInput}
						ref={telRef}
					/>
					<label htmlFor="" className="email-label">
						Введите ваше сообщение
					</label>
					<textarea
						className={messageTextareaClassName}
						name="message-textarea"
						onFocus={onFocusMessageTextarea}
						onBlur={onBlurMessageTextarea}
						cols={100}
						ref={messageTextareaRef}
					/>
					<div className="d-flex justify-content-center">
						<ReCAPTCHA ref={recaptchaRef} sitekey={PUBLIC_RECAPTCHA_KEY} onChange={onCaptchaChange} />
					</div>
					<button
						className={captchaVerified ? "bg-primary submit-btn" : "bg-secondary submit-btn"}
						type="submit"
						disabled={!captchaVerified}
					>
						Отправить
					</button>
				</div>
			)}
			{chatStore.sse && (
				<div className="bottom-div">
					<div className="chat-wrapper">
						<div className="chat-container">
							<section className="chat-interface">
								{chatStore.messages.map((message) => {
									const fileName = message.fileName ? message.fileName.split(".") : null;
									return (
										<figure
											key={message.id}
											className={
												message.type === "image" || message.type === "document"
													? `chat-item ${message.messageAuthor} files`
													: `chat-item ${message.messageAuthor}`
											}
										>
											<figcaption className="chat-item chat-item_body">
												{message.operatorName && (
													<div className="authorMessageName">
														<i className="fa-solid fa-circle-user account_user"></i>
														<div className="nameElement">{message.operatorName.fullName}</div>
													</div>
												)}
												{message.clientName && (
													<div className="authorMessageName">
														<i className="fa-solid fa-circle-user account_user"></i>
														<div className="nameElement">{message.clientName.fullName}</div>
													</div>
												)}
												<div className="message-body-text">
													{message.body}
													{message.time && <div className="message_time">{message.time}</div>}
													{message.type === "image" && (
														<a
															href={`${axelorDomain}/ws/public/file/${message.messageSecretKey}`}
															target="_blank"
															rel="noreferrer"
														>
															<img
																src={`${axelorDomain}/ws/public/file/${message.messageSecretKey}`}
																alt=""
																className="message_image"
															/>
														</a>
													)}
													{message.type === "image" && message.status && (
														<a href={String(message.fileUrlArray)} target="_blank" rel="noreferrer">
															<img src={String(message.fileUrlArray)} alt="" className="message_image" />
														</a>
													)}
													{message.type === "image" && !message.status && (
														<a
															href={`${axelorDomain}/ws/public/download/${message.messageSecretKey}`}
															className="btn_download"
															download
														>
															<i className="fa-regular fa-circle-down downloadIcon"></i>
														</a>
													)}

													{message.type === "document" && (
														<div className="message_block">
															<div className="message_block_item">
																<i className="fa-solid fa-file description_file"></i>
															</div>
															<div className="message_block_item">
																<a
																	href={`${axelorDomain}/ws/public/file/${message.messageSecretKey}`}
																	target="_blank"
																	rel="noreferrer"
																>
																	<div className="message_children_block">
																		<div className="message_children_block_item">{message?.fileName}</div>
																		<div className="message_children_block_item">{`${fileName?.length > 0 && fileName[1]} * ${message.fileSize}`}</div>
																	</div>
																</a>
															</div>
														</div>
													)}

													{message.type === "document" && !message.status && (
														<a
															href={`${axelorDomain}/ws/public/download/${message.messageSecretKey}`}
															className="btn_download"
															download
														>
															<i className="fa-regular fa-circle-down downloadIcon"></i>
														</a>
													)}
												</div>
												{(message.type === "text" || message.type === "image" || message.type === "document") &&
													message.messageAuthor === "client" && (
														<div className="figcaption-preloader">
															{message.status ? (
																<i className="fa-solid fa-check check-icon"></i>
															) : (
																<img src={appStore.imageLinks?.doubleCheck} alt="" />
															)}
														</div>
													)}
											</figcaption>
										</figure>
									);
								})}
							</section>
						</div>
						<section className="new-message">
							<Emojis inputValue={inputValue} setInputValue={setInputValue} />
							<AttachingFile setMessageFileUrl={setMessageFileUrl} setFile={setFile} />
							<textarea
								className="message-body"
								placeholder="Введите сообщение"
								autoFocus
								value={inputValue}
								onChange={(e) => setInputValue(e.target.value)}
								onKeyUp={onKeyUpInput}
							/>
							<button type="button" className={btnSendMessageClassName} onClick={onClickSendMessage}>
								<i className="fa-solid fa-arrow-up arrowUpIcon"></i>
							</button>
						</section>
						{messageFileUrl && (
							<div className="selected_file">
								<div className="wrap-span">
									<span className="close-selected-file" onClick={onClickCloseFile}>
										✖
									</span>
								</div>
								{file && file?.type.includes("image") ? (
									<img src={String(messageFileUrl)} alt="" className="message_file_img" />
								) : (
									<i className="fa-solid fa-file description_file"></i>
								)}
								<div>
									<div className="info_file">
										<abbr title={file && file.name} style={{ textDecoration: "none" }}></abbr>
										{file && (
											<p>
												{file.type.includes("image")
													? `${file.type} * ${file.size} KB`
													: `${file.name.split(".").reverse()[0]} * ${file.size} KB`}
											</p>
										)}
									</div>
									<button className="file-button" onClick={onClickSendFile}>
										Отправить
									</button>
								</div>
							</div>
						)}
					</div>
				</div>
			)}

			<button className="btn_messenger btn_closechat" type="button" onClick={onClickCloseForm}>
				<i className="fa-solid fa-xmark closeChatIcon"></i>
			</button>
			{isLoading === "pending" && <WrapSpinner />}
			{isLoading === "continue" && !chatStore.sse && <ContinueDiv />}
			{isLoading === "error" && <ErrorForm />}
		</form>
	);
}

function ContinueDiv() {
	return <div className="continueDiv"></div>;
}

function ErrorForm() {
	return (
		<div className="error-div">
			<h4 className="error-text">Произошла ошибка</h4>
		</div>
	);
}

function WrapSpinner() {
	return (
		<div className="wrapSpinner">
			<img
				src="https://cdn.glitch.global/c6949531-6864-4a24-9764-af86d4acd908/spinner.gif?v=1698138487857"
				alt=""
				className="spinner"
			/>
		</div>
	);
}

function Emojis({ inputValue, setInputValue }: { inputValue: string; setInputValue: (value: string) => void }) {
	const [show, setShow] = useState<boolean>(false);
	const emojisArray: string[] = [
		"😀",
		"😃",
		"😄",
		"😁",
		"😆",
		"😅",
		"😂",
		"🤣",
		"😊",
		"😇",
		"🙂",
		"🙃",
		"😉",
		"😌",
		"😍",
		"😘",
		"😗",
		"😙",
		"😚",
		"☺️",
		"🤗",
		"🤔",
		"😐",
		"😑",
		"😶",
		"🙄",
		"😏",
		"😣",
		"😥",
		"😮",
	];

	const onClickEmojis = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		e.stopPropagation();
		setShow(!show);
	};
	const onClickEmoji = (emojis: string) => {
		setInputValue(inputValue + emojis);
	};
	return (
		<div className="emojis_container">
			{show && (
				<div className="emojisShow">
					<ul className="emojis_ul">
						{emojisArray.map((emojis) => {
							return (
								<li key={emojis} onClick={() => onClickEmoji(emojis)}>
									{emojis}
								</li>
							);
						})}
					</ul>
				</div>
			)}
			<button className="btnShowEmojis" type="button" onClick={onClickEmojis}>
				{show ? <i className="fa-solid fa-xmark closeSmile"></i> : <i className="fa-solid fa-face-smile smileIcon"></i>}
			</button>
		</div>
	);
}
