import dayjs from "dayjs";

export interface MessageType {
	body: string;
	clientName?: {
		fullName: string;
		id: number;
	} | null;
	fileId?: string | null;
	fileName?: string | null;
	fileSize?: string | null;
	fileType?: string | null;
	fileUrlArray?: string | ArrayBuffer | null;
	fromNumber?: string;
	id: number;
	messageAuthor: string;
	messageSecretKey?: string | null;
	operatorName?: {
		fullName: string;
		id: number;
	} | null;
	time?: string;
	timestamp?: number;
	status?: string;
	type: string;
}
export interface UserType {
	userPhone: string;
	userName: string;
	id: number;
}
interface FileTypes {
	audio: {
		type: string;
		filestypes: [string, string, string, string, string, string];
	};
	document: {
		type: string;
		filestypes: [string, string, string, string, string, string, string, string, string, string];
	};
	image: {
		type: string;
		filestypes: [string, string];
	};
	sticker: {
		type: string;
		filestypes: [string];
	};
}

interface FileTypesArrType {
	fileTypes: string;
	type: string;
}

interface GetTypesArrObjType {
	filestypes: string[];
	type: string;
}

export function fileTypeService(fileType: string) {
	const fileTypes: FileTypes = {
		audio: {
			type: "audio",
			filestypes: ["audio/aac", "audio/mp4", "audio/mpeg", "audio/amr", "audio/ogg", "audio/webm"],
		},
		document: {
			type: "document",
			filestypes: [
				"text/plain",
				"application/pdf",
				"application/vnd.ms-powerpoint",
				"application/msword",
				"application/vnd.ms-excel",
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
				"application/vnd.openxmlformats-officedocument.presentationml.presentation",
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
				"application/vnd.ms-publisher",
				"text/csv",
			],
		},
		image: {
			type: "image",
			filestypes: ["image/jpeg", "image/png"],
		},
		sticker: {
			type: "sticker",
			filestypes: ["image/webp"],
		},
	};
	const fileTypeArr = getFileType({ fileTypes });
	function getType(fileType: string) {
		let type: string | null = null;
		for (let i = 0; i < fileTypeArr.length; i++) {
			const item: FileTypesArrType = fileTypeArr[i];
			if (item.fileTypes === fileType) {
				type = item.type;
				break;
			} else {
				type = "document";
			}
		}
		return type;
	}
	function getTypesArr(obj: GetTypesArrObjType) {
		const arr: FileTypesArrType[] = [];
		for (let i = 0; i < obj.filestypes.length; i++) {
			arr.push({ fileTypes: obj.filestypes[i], type: obj.type });
		}
		return arr;
	}
	function getFileType({ fileTypes }) {
		const { image, document, sticker, audio } = fileTypes;
		let arr: FileTypesArrType[] = [];
		const imageTypes = getTypesArr(image);
		const documentTypes = getTypesArr(document);
		const stickerTypes = getTypesArr(sticker);
		const audioTypes = getTypesArr(audio);
		arr = [...imageTypes, ...documentTypes, ...stickerTypes, ...audioTypes];
		return arr;
	}

	return getType(fileType);
}

export const domain: string = "https://educated-adhesive-origami.glitch.me";
export const axelorDomain: string = "https://call.sanarip.org/axelor-erp";
export const token: string =
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJTRUNSRVRHVFMiOiJTRUNSRVRHVFMifQ.evaOkks9XaPgrWmzSgBbi1In3ucsqMsSfOWSCXEnMS8";

export function createTimeStamp() {
	return dayjs().unix();
}

export function parseTimeStamp(timeStamp: number) {
	const date = dayjs.unix(timeStamp);
	const today = dayjs();

	if (date.isSame(today, "day")) {
		return `${date.format("HH:mm")}`;
	} else {
		return date.format("DD.MM.YYYY HH:mm:ss"); // Форматирование даты и времени
	}
}
