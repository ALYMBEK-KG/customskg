import React, { useState } from "react";
import { useAppStore } from "../../stores";
import GetIcons from "./elements/get-icons";
import "./style.scss";

export default function Component() {
	const [showIcons, setShowIcons] = useState<boolean>(false);
	const [classNameWidget, setClassNameWidget] = useState<string>("widget_container");
	const appStore = useAppStore((state) => state);

	const onClickBtnMessenger = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		e.stopPropagation();
		setShowIcons(true);
		setClassNameWidget("widget_container widget_container__open");
	};

	return (
		<div className={classNameWidget}>
			{showIcons && <GetIcons setShowIcons={setShowIcons} setClassNameWidget={setClassNameWidget} />}
			{!showIcons && (
				<button className="btn_messenger btn_open" onClick={onClickBtnMessenger}>
					<span className="btn_onlinechat_background">
						<img src={appStore.imageLinks?.chatIcon} alt="" className="chatIcon" />
					</span>
				</button>
			)}
		</div>
	);
}
