import { User } from "../models";
import type { LinkDataList } from "/site/content-types/link/link.d";

export type AppData = {
	appName: string;
	sitePath: { title: string; path: string; link: string }[];
	siteData: Record<string, string>;
	locale: string;
	locales: { locale: string; link: string }[];
	phrases: Record<string, string>;
	rootPage: string;
	currentPage: string;
	navMenuList: LinkDataList;
	alerts?: {
		title: string;
		link?: string;
		type?: "danger" | "warning" | "success" | "info";
	}[];
	authData?: { cookie?: string; user?: User };
	imageLinks?: Record<string, string>;
	isHeaderTransparent?: boolean;
};
