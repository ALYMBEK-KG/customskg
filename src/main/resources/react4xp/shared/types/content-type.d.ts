export type ContentType = {
	contentTypeLink?: string;
	contentTypeImage?: string;
	contentTypeImageAltText?: string;
};
