import "./index.scss";

import { Form, Formik } from "formik";
import React, { useCallback, useState } from "react";
import Select, { StylesConfig } from "react-select";
import { useAppStore } from "../../stores";
import { useEffectOnce } from "/react4xp/shared/hooks";
import { ImportCalculator, importCalculator } from "/react4xp/shared/models";
import { http, isRequiredField } from "/react4xp/shared/utils/client";

const INITIAL_VALUES: ImportCalculator = {
	currency: null,
	amount: null,
	customsValue: null,
	amountUnits: null,
	departureCountry: null,
	originCountry: null,
	tnved: null,
};

const selectStyles = (touched: boolean, errors: string): StylesConfig<{ label: string; value: string }, false> => ({
	container: (provided) => ({ ...provided, width: "100%" }),
	menu: (provided) => ({ ...provided, zIndex: 5 }),
	control: (provided) => ({
		...provided,
		width: "100%",
		minHeight: "38px",
		border: `var(--bs-border-width) solid ${touched && errors ? "var(--bs-form-invalid-border-color)" : "var(--bs-border-color)"}`,
		borderRadius: "var(--bs-border-radius)",
	}),
});

export default function Component() {
	const appStore = useAppStore((state) => state);
	const appStoreAlert = useAppStore((state) => state.alert);

	const [countries, setCountries] = useState<{ code: string; name: string }[]>([]);
	const [units, setUnits] = useState<{ code: string; name: string }[]>([]);
	const [currencies, setCurrencies] = useState<{ code: string; name: string }[]>([]);

	const [data, setData] = useState<ImportCalculator>(null);

	const handleSubmit = (values: ImportCalculator) => {
		setData(values);
		console.log(values);
	};

	const formatTnvedInput = (value: string) => {
		const cleaned = value?.replace(/\D/g, "");
		const match = cleaned?.match(/^(\d{0,6})(\d{0,2})(\d{0,2})$/);

		return match ? [match[1], match[2], match[3]].filter(Boolean).join(" ") : value;
	};

	const isFieldRequired = (field: string, values?: ImportCalculator) => {
		return isRequiredField({ schema: importCalculator, field, values });
	};

	const fetchDictionaries = useCallback(async () => {
		if (!appStore.sitePath || !appStore.appName) return;

		try {
			const [unitsRes, countriesRes, currenciesRes] = await Promise.all([
				http<typeof units>({ url: `${appStore.sitePath[0].link}/_/service/${appStore.appName}/units` }),
				http<typeof countries>({ url: `${appStore.sitePath[0].link}/_/service/${appStore.appName}/countries` }),
				http<typeof currencies>({
					url: `${appStore.sitePath[0].link}/_/service/${appStore.appName}/currency-dictionary`,
				}),
			]);

			setCountries(countriesRes.jsonData);
			setUnits(unitsRes.jsonData);
			setCurrencies(currenciesRes.jsonData);
		} catch (e) {
			appStoreAlert({ type: "danger", title: appStore.phrases?.["anUnexpectedErrorOccurred"] });
		}
	}, [appStore.sitePath, appStore.appName, appStoreAlert]);

	useEffectOnce(() => {
		fetchDictionaries();
	}, [fetchDictionaries]);

	return (
		<>
			<Formik initialValues={INITIAL_VALUES} validationSchema={importCalculator} onSubmit={handleSubmit}>
				{({ touched, errors, values, handleBlur, handleChange }) => (
					<div>
						<div className="alert alert-primary" role="alert">
							<h5 className="fw-bolder m-0">{appStore.phrases?.["import"]}</h5>
						</div>

						<Form>
							<div className="import-calculator">
								<div>
									<label
										className={`form-label fw-semibold ${isFieldRequired("tnved") ? "required" : ""}`}
										htmlFor="tnved"
									>
										{appStore.phrases?.["codeTnVed"]}
									</label>
									<div className="input-group">
										<input
											type="text"
											maxLength={12}
											name="tnved"
											id="tnved"
											className={`form-control ${touched.tnved && errors.tnved ? "is-invalid" : ""}`}
											value={formatTnvedInput(values.tnved)}
											onChange={handleChange}
										/>
										{touched.tnved && errors.tnved && (
											<div className="invalid-feedback">{appStore.phrases?.[errors.tnved]}</div>
										)}
									</div>
								</div>
								<div>
									<label
										className={`form-label fw-semibold ${isFieldRequired("originCountry") ? "required" : ""}`}
										htmlFor="originCountry"
									>
										{appStore.phrases?.["countryOfOrigin"]}
									</label>
									<div>
										<Select
											name="originCountry"
											id="originCountry"
											placeholder=""
											styles={selectStyles(touched.originCountry, errors.originCountry)}
											options={countries.map((country) => ({
												value: country.code,
												label: country.name,
											}))}
											onBlur={handleBlur}
											onChange={(selectedOption) => {
												handleChange({
													target: {
														name: "originCountry",
														value: selectedOption ? selectedOption.value : "",
													},
												});
											}}
											isSearchable
										/>
										{touched.originCountry && errors.originCountry && (
											<div className="errorMsg">{appStore.phrases?.[errors.originCountry]}</div>
										)}
									</div>
								</div>
								<div>
									<label
										className={`form-label fw-semibold ${isFieldRequired("departureCountry") ? "required" : ""}`}
										htmlFor="departureCountry"
									>
										{appStore.phrases?.["departureCountry"]}
									</label>
									<div className="input-group">
										<Select
											name="departureCountry"
											id="departureCountry"
											placeholder=""
											styles={selectStyles(touched.departureCountry, errors.departureCountry)}
											options={countries.map((country) => ({
												value: country.code,
												label: country.name,
											}))}
											onChange={(selectedOption) => {
												handleChange({
													target: {
														name: "departureCountry",
														value: selectedOption ? selectedOption.value : "",
													},
												});
											}}
											isSearchable
										/>
										{touched.departureCountry && errors.departureCountry && (
											<div className="errorMsg">{appStore.phrases?.[errors.departureCountry]}</div>
										)}
									</div>
								</div>
								<div>
									<label
										className={`form-label fw-semibold ${isFieldRequired("customsValue") ? "required" : ""}`}
										htmlFor="customsValue"
									>
										{appStore.phrases?.["customsValue"]}
									</label>
									<div className="input-group">
										<input
											type="number"
											name="customsValue"
											id="customsValue"
											className={`form-control ${touched.customsValue && errors.customsValue ? "is-invalid" : ""}`}
											value={values.customsValue}
											onChange={handleChange}
										/>
										{touched.customsValue && errors.customsValue && (
											<div className="invalid-feedback">{appStore.phrases?.[errors.customsValue]}</div>
										)}
									</div>
								</div>
								<div>
									<label
										className={`form-label fw-semibold ${isFieldRequired("currency") ? "required" : ""}`}
										htmlFor="currency"
									>
										{appStore.phrases?.["currency"]}
									</label>
									<Select
										name="currency"
										id="currency"
										placeholder=""
										styles={selectStyles(touched.currency, errors.currency)}
										options={currencies.map((country) => ({
											value: country.code,
											label: country.name,
										}))}
										onChange={(selectedOption) => {
											handleChange({
												target: {
													name: "currency",
													value: selectedOption ? selectedOption.value : "",
												},
											});
										}}
										isSearchable
									/>
									{touched.currency && errors.currency && (
										<div className="errorMsg">{appStore.phrases?.[errors.currency]}</div>
									)}
								</div>
								<div>
									<label
										className={`form-label fw-semibold ${isFieldRequired("amount") ? "required" : ""}`}
										htmlFor="amount"
									>
										{appStore.phrases?.["amount"]}
									</label>
									<div className="input-group">
										<input
											type="number"
											name="amount"
											id="amount"
											className={`form-control ${touched.amount && errors.amount ? "is-invalid" : ""}`}
											value={values.amount}
											onChange={handleChange}
										/>
										{touched.amount && errors.amount && (
											<div className="invalid-feedback">{appStore.phrases?.[errors.amount]}</div>
										)}
									</div>
								</div>
								<div>
									<label
										className={`form-label fw-semibold ${isFieldRequired("amountUnits") ? "required" : ""}`}
										htmlFor="amountUnits"
									>
										{appStore.phrases?.["amountUnits"]}
									</label>
									<div className="input-group">
										<Select
											name="amountUnits"
											id="amountUnits"
											placeholder=""
											styles={selectStyles(touched.amountUnits, errors.amountUnits)}
											options={units.map((country) => ({
												value: country.code,
												label: country.name,
											}))}
											onChange={(selectedOption) => {
												handleChange({
													target: {
														name: "amountUnits",
														value: selectedOption ? selectedOption.value : "",
													},
												});
											}}
											isSearchable
										/>
										{touched.amountUnits && errors.amountUnits && (
											<div className="errorMsg">{appStore.phrases?.[errors.amountUnits]}</div>
										)}
									</div>
								</div>

								<div>
									<label
										className={`form-label fw-semibold ${isFieldRequired("weight") ? "required" : ""}`}
										htmlFor="weight"
									>
										{appStore.phrases?.["weight"]}
									</label>
									<div className="input-group">
										<input
											type="number"
											name="weight"
											id="weight"
											className={`form-control ${touched.weight && errors.weight ? "is-invalid" : ""}`}
											value={values.weight}
											onChange={handleChange}
										/>
										{touched.weight && errors.weight && (
											<div className="invalid-feedback">{appStore.phrases?.[errors.weight]}</div>
										)}
									</div>
								</div>
							</div>

							<div className="d-flex justify-content-end mt-4">
								<button className="btn btn-primary" type="submit">
									{appStore.phrases?.["calculate"]}
								</button>
							</div>
						</Form>
					</div>
				)}
			</Formik>
			{data && (
				<div className="table-responsive mt-5">
					<table className="table table-bordered table-hover caption-top">
						<caption className="fs-3 fw-bold alert alert-primary">
							{appStore.phrases?.["expectedCalculatedCustomsPayments"]}
						</caption>
						<thead className="thead-light">
							<tr>
								<th scope="col">{appStore.phrases?.["type"]}</th>
								<th scope="col">{appStore.phrases?.["rate"]}</th>
								<th scope="col">{appStore.phrases?.["basis"]}</th>
								<th scope="col">
									{appStore.phrases?.["sum"]} ({data.currency})
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">{appStore.phrases?.["customsFee"]}</th>
								<td scope="row">0,4%</td>
								<td scope="row">ЗТР</td>
								<td scope="row">{(data.customsValue * 0.004).toFixed(2)}</td>
							</tr>
							<tr>
								<th scope="row">{appStore.phrases?.["customsDuty"]}</th>
								<td scope="row">15%</td>
								<td scope="row">ЕТТ</td>
								<td scope="row">{(data.customsValue * 0.15).toFixed(2)}</td>
							</tr>
							<tr>
								<th scope="row">{appStore.phrases?.["vat"]}</th>
								<td scope="row">12%</td>
								<td scope="row">НК КР</td>
								<td scope="row">{(data.customsValue * 0.12).toFixed(2)}</td>
							</tr>
							<tr>
								<th scope="row">{appStore.phrases?.["exciseDuty"]}</th>
								<td scope="row"></td>
								<td scope="row"></td>
								<td scope="row"></td>
							</tr>
							<tr>
								<th scope="row">{appStore.phrases?.["specialDuty"]}</th>
								<td scope="row"></td>
								<td scope="row"></td>
								<td scope="row"></td>
							</tr>
							<tr>
								<th scope="row">{appStore.phrases?.["antidumpingDuty"]}</th>
								<td scope="row"></td>
								<td scope="row"></td>
								<td scope="row"></td>
							</tr>
							<tr>
								<th scope="row">{appStore.phrases?.["compensationDuty"]}</th>
								<td scope="row"></td>
								<td scope="row"></td>
								<td scope="row"></td>
							</tr>
							<tr>
								<th scope="row">{appStore.phrases?.["other"]}</th>
								<td scope="row"></td>
								<td scope="row"></td>
								<td scope="row"></td>
							</tr>
							<tr>
								<th scope="row">{appStore.phrases?.["result"]}</th>
								<td scope="row"></td>
								<td scope="row"></td>
								<td scope="row">
									{(data.customsValue * 0.004 + data.customsValue * 0.15 + data.customsValue * 0.12).toFixed(2)}
								</td>
							</tr>
						</tbody>
					</table>
					<table className="table table-bordered table-hover mt-4 caption-top">
						<caption className="fs-3 fw-bold alert alert-primary">
							{appStore.phrases?.["docsTariffRegMeasures"]}
						</caption>
						<thead className="thead-light">
							<tr>
								<th scope="col">{appStore.phrases?.["orgName"]}</th>
								<th scope="col">{appStore.phrases?.["docName"]}</th>
								<th scope="col">{appStore.phrases?.["NLA"]}</th>
								<th scope="col">{appStore.phrases?.["docType"]}</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td scope="row">1</td>
								<td scope="row">2</td>
								<td scope="row">3</td>
								<td scope="row">4</td>
							</tr>
							<tr>
								<td scope="row">1</td>
								<td scope="row">2</td>
								<td scope="row">3</td>
								<td scope="row">4</td>
							</tr>
							<tr>
								<td scope="row">1</td>
								<td scope="row">2</td>
								<td scope="row">3</td>
								<td scope="row">4</td>
							</tr>
							<tr>
								<td scope="row">1</td>
								<td scope="row">2</td>
								<td scope="row">3</td>
								<td scope="row">4</td>
							</tr>
							<tr>
								<td scope="row">1</td>
								<td scope="row">2</td>
								<td scope="row">3</td>
								<td scope="row">4</td>
							</tr>
							<tr>
								<td scope="row">1</td>
								<td scope="row">2</td>
								<td scope="row">3</td>
								<td scope="row">4</td>
							</tr>
							<tr>
								<td scope="row">1</td>
								<td scope="row">2</td>
								<td scope="row">3</td>
								<td scope="row">4</td>
							</tr>
						</tbody>
					</table>
					<table className="table table-bordered table-hover mt-4 caption-top">
						<caption className="fs-3 fw-bold alert alert-primary">{appStore.phrases?.["productRelInfo"]}</caption>
						<tbody>
							<tr>
								<td>
									1)С момента прибытия уч. ВЭД обязан уведомить ТО в течении 1 часа (согласно пп. 1 п. 1 ст. 88 ТК ЕАЭС)
								</td>
							</tr>
							<tr>
								<td>
									2)После уведомления ТО уч. Вэд обязан в течении 3 часов рабочего времени таможенного органа совершить
									одну из таможенных операций, связанных с: (доп вкладка переченеть операций п.5 статьи 88 ТК ЕАЭС)
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			)}
		</>
	);
}
