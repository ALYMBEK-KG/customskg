import "./index.scss";

import { Field, Form, Formik } from "formik";
import React, { useState } from "react";

import { signUp, SignUp } from "../../../models";
import { useAppStore } from "../../../stores";
import { http, isRequiredField } from "../../../utils/client";

export default function Component() {
	const [error, setError] = useState<boolean>(false);
	const [success, setSuccess] = useState<boolean>(false);

	const appStore = useAppStore((state) => state);

	const isFieldRequired = (field: string, values?: SignUp) => {
		return isRequiredField({ schema: signUp, field, values });
	};

	const handleSubmit = async (values: SignUp) => {
		if (appStore.sitePath == null || appStore.appName == null || appStore.authData != null) return;

		setError(false);

		try {
			const response = await http({
				url: `${appStore.sitePath[0].link}/_/service/${appStore.appName}/sign-up`,
				method: "POST",
				body: JSON.stringify(values ?? "null"),
			});

			const jsonData = response.jsonData as Record<string, unknown>;

			if (jsonData?.status === 0) {
				setSuccess(true);
			} else {
				setError(true);
			}
		} catch (e) {
			setError(true);
		}
	};

	return (
		<Formik
			initialValues={{
				email: "",
				privacyAgreement: true,
				typeSelect: 2,
				personalNumber: "",
				taxPayerId: "",
				firstName: "",
				lastName: "",
				birthDate: undefined,
			}}
			validationSchema={signUp}
			onSubmit={handleSubmit}
		>
			{({ errors, touched, values, initialValues, resetForm }) => (
				<Form>
					<div className="mb-3">
						<div className="form-check form-check-inline">
							<Field
								type="radio"
								name="typeSelect"
								className="form-check-input"
								id="sign-up__typeSelect1"
								value={2}
								checked={values.typeSelect == 2}
								onChange={(e) => resetForm({ values: { ...initialValues, typeSelect: e.target.value } })}
							/>

							<label className="form-check-label" htmlFor="sign-up__typeSelect1">
								{appStore.phrases?.["individual"]}
							</label>
						</div>

						<div className="form-check form-check-inline">
							<Field
								type="radio"
								name="typeSelect"
								className="form-check-input"
								id="sign-up__typeSelect2"
								value={1}
								checked={values.typeSelect == 1}
								onChange={(e) => resetForm({ values: { ...initialValues, typeSelect: e.target.value } })}
							/>

							<label className="form-check-label" htmlFor="sign-up__typeSelect2">
								{appStore.phrases?.["legalEntity"]}
							</label>
						</div>
					</div>

					{values.typeSelect == 1 && (
						<>
							<div className="mb-3">
								<label
									className={`form-label ${isFieldRequired("taxPayerId", values) ? "required" : ""}`}
									htmlFor="sign-up__tin"
								>
									{appStore.phrases?.["tin"]}
								</label>

								<Field
									type="text"
									name="taxPayerId"
									className={`form-control ${touched.taxPayerId ? (errors.taxPayerId ? "is-invalid" : "is-valid") : ""}`}
									id="sign-up__tin"
									placeholder="10101200000000"
								/>

								{touched.taxPayerId && errors.taxPayerId && (
									<div className="invalid-feedback">{appStore.phrases?.[errors.taxPayerId]}</div>
								)}
							</div>
						</>
					)}

					{values.typeSelect == 2 && (
						<>
							<div className="mb-3">
								<label
									className={`form-label ${isFieldRequired("firstName", values) ? "required" : ""}`}
									htmlFor="sign-up__firstName"
								>
									{appStore.phrases?.["firstName"]}
								</label>

								<Field
									type="text"
									name="firstName"
									className={`form-control ${touched.firstName ? (errors.firstName ? "is-invalid" : "is-valid") : ""}`}
									id="sign-up__firstName"
									placeholder="Исмаил"
								/>

								{touched.firstName && errors.firstName && (
									<div className="invalid-feedback">{appStore.phrases?.[errors.firstName]}</div>
								)}
							</div>

							<div className="mb-3">
								<label
									className={`form-label ${isFieldRequired("lastName", values) ? "required" : ""}`}
									htmlFor="sign-up__lastName"
								>
									{appStore.phrases?.["lastName"]}
								</label>

								<Field
									type="text"
									name="lastName"
									className={`form-control ${touched.lastName ? (errors.lastName ? "is-invalid" : "is-valid") : ""}`}
									id="sign-up__lastName"
									placeholder="Исмаилов"
								/>

								{touched.lastName && errors.lastName && (
									<div className="invalid-feedback">{appStore.phrases?.[errors.lastName]}</div>
								)}
							</div>

							<div className="mb-3">
								<label
									className={`form-label ${isFieldRequired("birthDate", values) ? "required" : ""}`}
									htmlFor="sign-up__birthDate"
								>
									{appStore.phrases?.["birthDate"]}
								</label>

								<Field
									type="date"
									name="birthDate"
									className={`form-control ${touched.birthDate ? (errors.birthDate ? "is-invalid" : "is-valid") : ""}`}
									id="sign-up__birthDate"
								/>

								{touched.birthDate && errors.birthDate && (
									<div className="invalid-feedback">{appStore.phrases?.[errors.birthDate as string]}</div>
								)}
							</div>

							<div className="mb-3">
								<label
									className={`form-label ${isFieldRequired("personalNumber", values) ? "required" : ""}`}
									htmlFor="sign-up__pin"
								>
									{appStore.phrases?.["pin"]}
								</label>

								<Field
									type="text"
									name="personalNumber"
									className={`form-control ${touched.personalNumber ? (errors.personalNumber ? "is-invalid" : "is-valid") : ""}`}
									id="sign-up__pin"
									placeholder="10101200000000"
								/>

								{touched.personalNumber && errors.personalNumber && (
									<div className="invalid-feedback">{appStore.phrases?.[errors.personalNumber]}</div>
								)}
							</div>
						</>
					)}

					<div className="mb-3">
						<label
							className={`form-label ${isFieldRequired("email", values) ? "required" : ""}`}
							htmlFor="sign-up__email"
						>
							{appStore.phrases?.["email"]}
						</label>

						<Field
							type="text"
							name="email"
							className={`form-control ${touched.email ? (errors.email ? "is-invalid" : "is-valid") : ""}`}
							id="sign-up__email"
							placeholder="ismail_ismailov@gmail.com"
						/>

						{touched.email && errors.email && (
							<div className="invalid-feedback">{appStore.phrases?.[errors.email]}</div>
						)}
					</div>

					<div className="form-check mb-3">
						<Field
							type="checkbox"
							name="privacyAgreement"
							className="form-check-input"
							id="sign-up__privacyAgreement"
							checked
						/>

						<label
							className={`form-check-label ${isFieldRequired("privacyAgreement", values) ? "required" : ""}`}
							htmlFor="sign-up__privacyAgreement"
						>
							<a href={`${appStore.sitePath?.[0].link}/privacy-agreement`}>{appStore.phrases?.["privacyAgreement"]}</a>
						</label>

						{touched.privacyAgreement && errors.privacyAgreement && (
							<div className="invalid-feedback">{appStore.phrases?.[errors.privacyAgreement]}</div>
						)}
					</div>

					{error && <p className="text-danger">{appStore.phrases?.["invalidCredentials"]}</p>}

					{success && (
						<p className="text-success">
							{appStore.phrases?.["requestSentSuccessfully"]}. {appStore.phrases?.["checkEmail"]}
						</p>
					)}

					<button type="submit" className="btn btn-primary">
						{appStore.phrases?.["signUp"]}
					</button>
				</Form>
			)}
		</Formik>
	);
}
