import "./index.scss";

import { Field, Form, Formik } from "formik";
import React, { useState } from "react";

import { SignIn, signIn } from "../../../models";
import { useAppStore } from "../../../stores";
import { ADMIN_BASE_URL, http, isRequiredField } from "../../../utils/client";

export default function Component() {
	const [authError, setAuthError] = useState<boolean>(false);

	const appStore = useAppStore((state) => state);
	const appStoreUpdate = useAppStore((state) => state.update);

	const isFieldRequired = (field: string, values?: SignIn) => {
		return isRequiredField({ schema: signIn, field, values });
	};

	const handleSubmit = async (values: SignIn) => {
		if (appStore.sitePath == null || appStore.appName == null || appStore.authData != null) return;

		setAuthError(false);

		try {
			const response = await http({
				url: `${appStore.sitePath[0].link}/_/service/${appStore.appName}/sign-in`,
				method: "POST",
				body: JSON.stringify(values ?? "null"),
			});

			if (response.jsonData) {
				appStoreUpdate({ authData: response.jsonData });
				location.href = ADMIN_BASE_URL;
			} else {
				setAuthError(true);
			}
		} catch (e) {
			setAuthError(true);
		}
	};

	return (
		<Formik
			initialValues={{ username: "", password: "", rememberMe: false }}
			validationSchema={signIn}
			onSubmit={handleSubmit}
		>
			{({ errors, touched }) => (
				<Form>
					<div className="mb-3">
						<label
							className={`form-label ${isFieldRequired("username") ? "required" : ""}`}
							htmlFor="sign-in__username"
						>
							{appStore.phrases?.["pin"]}
						</label>

						<Field
							type="text"
							name="username"
							className={`form-control ${touched.username ? (errors.username ? "is-invalid" : "is-valid") : ""}`}
							id="sign-in__username"
							placeholder="10101200000000"
						/>

						{touched.username && errors.username && (
							<div className="invalid-feedback">{appStore.phrases?.[errors.username]}</div>
						)}
					</div>

					<div className="mb-3">
						<label
							className={`form-label ${isFieldRequired("password") ? "required" : ""}`}
							htmlFor="sign-in__password"
						>
							{appStore.phrases?.["password"]}
						</label>

						<Field
							type="password"
							name="password"
							className={`form-control ${touched.password ? (errors.password ? "is-invalid" : "is-valid") : ""}`}
							id="sign-in__password"
							placeholder="******"
						/>

						{touched.password && errors.password && (
							<div className="invalid-feedback">{appStore.phrases?.[errors.password]}</div>
						)}
					</div>

					<div className="form-check mb-3">
						<label
							className={`form-check-label ${isFieldRequired("rememberMe") ? "required" : ""}`}
							htmlFor="sign-in__rememberMe"
						>
							{appStore.phrases?.["rememberMe"]}
						</label>

						<Field
							type="checkbox"
							name="rememberMe"
							className={`form-check-input ${touched.rememberMe ? (errors.rememberMe ? "is-invalid" : "is-valid") : ""}`}
							id="sign-in__rememberMe"
						/>

						{touched.rememberMe && errors.rememberMe && (
							<div className="invalid-feedback">{appStore.phrases?.[errors.rememberMe]}</div>
						)}
					</div>

					{authError && <p className="text-danger">{appStore.phrases?.["invalidCredentials"]}</p>}

					<button type="submit" className="btn btn-primary">
						{appStore.phrases?.["signIn"]}
					</button>
				</Form>
			)}
		</Formik>
	);
}
