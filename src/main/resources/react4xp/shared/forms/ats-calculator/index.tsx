import "./index.scss";

import { Form, Formik } from "formik";
import React, { useState } from "react";
import { AtsCalculator, atsCalculator } from "/react4xp/shared/models";
import { useAppStore } from "/react4xp/shared/stores";
import { CURRENCIES, http, isRequiredField } from "/react4xp/shared/utils/client";

export default function Component() {
	const appStore = useAppStore((state) => state);
	const appStoreAlert = useAppStore((state) => state.alert);

	const [data, setData] = useState<Record<string, string>[]>([]);

	const handleSubmit = async (values: AtsCalculator) => {
		const { calculationType, ...otherValues } = values;

		const data = {
			...otherValues,
			engineType: calculationType === "SIMPLE" ? "SIMPLE" : values.engineType,
		};

		setData([]);

		const calculatorResponse = await http<{ errors?: { message: string }; data: Record<string, string>[] }>({
			url: `${appStore.sitePath?.[0].link}/_/service/${appStore.appName}/ats-calculator`,
			method: "POST",
			body: JSON.stringify(data ?? "null"),
		});

		if (calculatorResponse.jsonData.data) {
			setData(calculatorResponse.jsonData.data);
		}

		if (!calculatorResponse.jsonData.data) {
			const errorMessage = calculatorResponse.jsonData?.errors?.message.replace(/\s+/g, "");
			const errorText = appStore?.phrases?.[errorMessage];

			appStoreAlert({ type: "danger", title: errorText });
		}
	};

	const formatNumber = (value: string) => value.replace(/\B(?=(\d{3})+(?!\d))/g, " ");

	const isFieldRequired = (field: string, values?: AtsCalculator) =>
		isRequiredField({ schema: atsCalculator, field, values });

	// ToDo
	// Код на реализацию международных валют
	// const [currencies, setCurrencies] = useState<{ code: string; name: string }[]>([]);
	// const appStoreAlert = useAppStore((state) => state.alert);
	// const fetchDictionaries = useCallback(async () => {
	// 	if (!appStore.sitePath || !appStore.appName) return;
	// 	try {
	// 		const [currenciesRes] = await Promise.all([
	// 			http<typeof currencies>({
	// 				url: `${appStore.sitePath[0].link}/_/service/${appStore.appName}/currency-dictionary`,
	// 			}),
	// 		]);
	// 		setCurrencies(currenciesRes.jsonData);
	// 	} catch (e) {
	// 		appStoreAlert({ type: "danger", title: appStore.phrases?.["anUnexpectedErrorOccurred"] });
	// 	}
	// }, [appStore.sitePath, appStore.appName, appStoreAlert]);
	// useEffectOnce(() => {
	// 	fetchDictionaries();
	// }, [fetchDictionaries]);

	return (
		<>
			<Formik
				initialValues={{
					currency: null,
					engineType: null,
					engineCapacity: null,
					calculationType: "COMMON",
					customsCost: null,
					manufactureYear: null,
				}}
				validationSchema={atsCalculator}
				onSubmit={handleSubmit}
			>
				{({ values, handleChange, handleBlur, errors, touched }) => (
					<Form>
						<div className="ats-calculator align-items-baseline">
							<div>
								<label
									className={`form-label fw-semibold ${isFieldRequired("calculationType") ? "required" : ""}`}
									htmlFor="calculationType"
								>
									{appStore.phrases?.["calculationType"]}
								</label>
								<select
									name="calculationType"
									id="calculationType"
									className={`form-select ${touched.calculationType && errors.calculationType ? "is-invalid" : ""}`}
									value={values.calculationType}
									onChange={handleChange}
									onBlur={handleBlur}
								>
									<option value="COMMON">{appStore.phrases?.["commonOrder"]}</option>
									<option value="SIMPLE">{appStore.phrases?.["forPersonalUse"]}</option>
								</select>
								{touched.calculationType && errors.calculationType && (
									<div className="invalid-feedback">{appStore.phrases?.[errors.calculationType]}</div>
								)}
							</div>
							<div>
								<label
									className={`form-label fw-semibold ${isFieldRequired("currency") ? "required" : ""}`}
									htmlFor="currency"
								>
									{appStore.phrases?.["currency"]}
								</label>
								<select
									name="currency"
									id="currency"
									className={`form-select ${touched.currency && errors.currency ? "is-invalid" : ""}`}
									value={values.currency}
									onChange={handleChange}
									onBlur={handleBlur}
								>
									<option value=""></option>
									{CURRENCIES.map((currency) => (
										<option key={currency} value={currency}>
											{currency}
										</option>
									))}
								</select>
								{touched.currency && errors.currency && (
									<div className="invalid-feedback">{appStore.phrases?.[errors.currency]}</div>
								)}
							</div>
							<div>
								<label
									className={`form-label fw-semibold ${isFieldRequired("engineCapacity") ? "required" : ""}`}
									htmlFor="engineCapacity"
								>
									{appStore.phrases?.["engineCapacity"]}
								</label>
								<div className="input-group">
									<input
										type="number"
										name="engineCapacity"
										id="engineCapacity"
										className={`form-control ${touched.engineCapacity && errors.engineCapacity ? "is-invalid" : ""}`}
										value={values.engineCapacity}
										onChange={handleChange}
										onBlur={handleBlur}
									/>
									<span className="input-group-text" id="engineCapacity">
										{appStore.phrases?.["centimeter"]}
										<sup>3</sup>
									</span>
									{touched.engineCapacity && errors.engineCapacity && (
										<div className="invalid-feedback">{appStore.phrases?.[errors.engineCapacity]}</div>
									)}
								</div>
							</div>
							<div>
								<label
									className={`form-label fw-semibold ${isFieldRequired("manufactureYear") ? "required" : ""}`}
									htmlFor="manufactureYear"
								>
									{appStore.phrases?.["manufactureYear"]}
								</label>
								<div className="input-group">
									<input
										type="number"
										name="manufactureYear"
										id="manufactureYear"
										className={`form-control ${touched.manufactureYear && errors.manufactureYear ? "is-invalid" : ""}`}
										value={values.manufactureYear}
										onChange={handleChange}
										onBlur={handleBlur}
									/>
									<span className="input-group-text" id="manufactureYear">
										{appStore.phrases?.["year"].toLowerCase()}
									</span>
									{touched.manufactureYear && errors.manufactureYear && (
										<div className="invalid-feedback">{appStore.phrases?.[errors.manufactureYear]}</div>
									)}
								</div>
							</div>
							<div>
								<label
									className={`form-label fw-semibold ${isFieldRequired("engineType") ? "required" : ""}`}
									htmlFor="engineType"
								>
									{appStore.phrases?.["engineType"]}
								</label>
								<select
									name="engineType"
									id="engineType"
									className={`form-select ${touched.engineType && errors.engineType ? "is-invalid" : ""}`}
									value={values.engineType}
									onChange={handleChange}
									onBlur={handleBlur}
								>
									<option value=""></option>
									<option value="ELECTRIC">{appStore.phrases?.["electricVehicle"].toLowerCase()}</option>
									<option value="DIESEL">
										{appStore.phrases?.["diesel"].toLowerCase()}, {appStore.phrases?.["dieselHybrid"].toLowerCase()}
									</option>
									<option value="PETROL">
										{appStore.phrases?.["petrol"].toLowerCase()}, {appStore.phrases?.["petrolHybrid"].toLowerCase()}
									</option>
								</select>
								{touched.engineType && errors.engineType && (
									<div className="invalid-feedback">{appStore.phrases?.[errors.engineType]}</div>
								)}
							</div>
							<div>
								<label
									className={`form-label fw-semibold ${isFieldRequired("customsCost") ? "required" : ""}`}
									htmlFor="customsCost"
								>
									{appStore.phrases?.["cost"]}
								</label>
								<div className="input-group">
									<input
										type="number"
										name="customsCost"
										id="customsCost"
										className={`form-control ${touched.customsCost && errors.customsCost ? "is-invalid" : ""}`}
										value={values.customsCost}
										onChange={handleChange}
										onBlur={handleBlur}
									/>
									{values.currency && (
										<span className="input-group-text" id="customsCost">
											{values.currency}
										</span>
									)}
									{touched.customsCost && errors.customsCost && (
										<div className="invalid-feedback">{appStore.phrases?.[errors.customsCost]}</div>
									)}
								</div>
							</div>
						</div>
						<div className="d-flex justify-content-end mt-3">
							<button className="btn btn-primary" type="submit">
								{appStore.phrases?.["calculate"]}
							</button>
						</div>
					</Form>
				)}
			</Formik>
			{data && data.length > 0 && (
				<div className="mt-3">
					<div className="alert alert-primary" role="alert">
						<h5 className="fw-bolder m-0">{appStore.phrases?.["customsDuties"]}</h5>
					</div>
					<table className="table">
						<thead>
							<tr>
								<th scope="col">{appStore.phrases?.["name"]}</th>
								{data.map((item, idx) => (
									<th scope="col" key={idx}>
										{appStore.phrases?.["amount"]} ({item.currencyCode})
									</th>
								))}
							</tr>
						</thead>
						<tbody>
							{data[0].customsServiceFees === "0" && data[0].vatDuty === "0" ? null : (
								<tr>
									<th scope="row">{appStore.phrases?.["customsClearanceFees"]}</th>
									{data.map((item) => (
										<td key={item.customsServiceFees}>{formatNumber(item.customsServiceFees)}</td>
									))}
								</tr>
							)}
							<tr>
								<th scope="row">
									{data[0].customsServiceFees === "0" && data[0].vatDuty === "0"
										? appStore.phrases?.["customsDutiesTaxes"]
										: appStore.phrases?.["importDuties"]}
								</th>
								{data.map((item) => (
									<td key={item.importCustomsDuty}>{formatNumber(item.importCustomsDuty)}</td>
								))}
							</tr>
							{data[0].customsServiceFees === "0" && data[0].vatDuty === "0" ? null : (
								<tr>
									<th scope="row">{appStore.phrases?.["vat"]}</th>
									{data.map((item) => (
										<td key={item.vatDuty}>{formatNumber(item.vatDuty)}</td>
									))}
								</tr>
							)}
							<tr>
								<th scope="row">{appStore.phrases?.["total"]}</th>
								{data.map((item) => (
									<td key={item.totalSum}>{formatNumber(item.totalSum)}</td>
								))}
							</tr>
						</tbody>
					</table>
				</div>
			)}
		</>
	);
}
