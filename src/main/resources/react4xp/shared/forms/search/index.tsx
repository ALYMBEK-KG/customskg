import "./index.scss";

import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import { Field, Form, Formik } from "formik";
import React, { ChangeEvent, useState } from "react";
import { useEvent } from "../../hooks";
import { useAppStore } from "../../stores";
import { http } from "../../utils/client";

let timerInstance: ReturnType<typeof setTimeout> | null = null;

export default function Component() {
	const [result, setResult] = useState<ContentsResult<Content>>(null);
	const [showResult, setShowResult] = useState<boolean>(false);

	const appStore = useAppStore((state) => state);
	const appStoreAlert = useAppStore((state) => state.alert);

	const handleSearchChange = (value: string) => {
		setResult(null);
		setShowResult(false);

		if (timerInstance) clearTimeout(timerInstance);
		if (appStore.sitePath == null || appStore.appName == null || value === "" || value.length < 2) return;

		timerInstance = setTimeout(async () => {
			try {
				const result = await http({
					url: `api/search?value=${value}`,
				});

				const jsonData = result.jsonData as ContentsResult<Content>;

				setResult(jsonData);
				setShowResult(true);
				clearTimeout(timerInstance);
			} catch (e) {
				appStoreAlert({ type: "danger", title: appStore.phrases?.["anUnexpectedErrorOccurred"] });
			}
		}, 500);
	};

	const handleFormSubmit = (values: { search: string }) => {
		if (values.search === "") {
			setShowResult(true);
			return;
		}

		location.href = `${appStore.sitePath?.[0].link}/search-results?search=${values.search}`;
	};

	useEvent(() => {
		setShowResult(false);
	});

	return (
		<Formik initialValues={{ search: "" }} onSubmit={handleFormSubmit}>
			{({ values, setFieldValue, handleChange }) => (
				<div className="global-search" onClick={(e) => e.stopPropagation()}>
					<Form>
						<div className="input-group">
							<Field
								type="text"
								name="search"
								className="form-control rounded-0"
								placeholder={appStore.phrases?.["searchForServicesAndInformation"]}
								onFocus={() => {
									if (result) setShowResult(true);
								}}
								onChange={(e: ChangeEvent<HTMLInputElement>) => {
									const value = e.target.value;
									handleSearchChange(value);
									handleChange(e);
								}}
							/>

							{values.search && (
								<button
									type="button"
									className="btn btn-clear"
									onClick={() => {
										setShowResult(false);
										setResult(null);
										setFieldValue("search", "");
									}}
								>
									<i className="fa-solid fa-xmark"></i>
								</button>
							)}

							<button type="submit" className="btn btn-search rounded-0">
								<i className="fa-solid fa-search"></i>
							</button>
						</div>
					</Form>

					{showResult && (
						<div className="global-search__content shadow">
							<div className="list-group">
								{result?.hits.length > 0 ? (
									<>
										{result.hits.map(({ _id, displayName, data }) => (
											<a
												key={`global-search_${_id}`}
												className="list-group-item list-group-item-action rounded-0 d-flex align-items-center gap-2"
												href={(data?.link as string) ?? (data?.contentTypeLink as string)}
											>
												<span
													className="list-group-item-image flex-shrink-0"
													style={{
														backgroundImage: `url("${data?.contentTypeImage}")`,
													}}
												>
													<span
														className="overlay"
														style={{ backgroundImage: `url("${appStore.imageLinks?.kgOrnament ?? ""}")` }}
													></span>
												</span>
												<span className="text-truncate">{displayName}</span>
											</a>
										))}

										<a
											className="list-group-item list-group-item-action btn text-truncate text-primary rounded-0"
											href={`${appStore.sitePath?.[0].link}/search-results?search=${values.search}`}
										>
											{appStore.phrases?.["showAll"]}
											<i className="fa-solid fa-arrow-right-long"></i>
										</a>
									</>
								) : (
									<div className="empty">
										<h3>{appStore.phrases?.["empty"]}</h3>
									</div>
								)}
							</div>
						</div>
					)}
				</div>
			)}
		</Formik>
	);
}
