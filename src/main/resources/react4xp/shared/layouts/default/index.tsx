import "./index.scss";

import React, { ReactNode, useEffect } from "react";

import { useAppStore } from "../../stores";
import type { AppData } from "../../types/app-data.d";

import Breadcrumb from "../../components/breadcrumb";
import Chat from "../../components/chat";
import Footer from "../../components/footer";
import Header from "../../components/header";
import Rate from "../../components/rate";
export default function Component({ appData, children }: { appData: AppData; children: ReactNode }) {
	const appStoreUpdate = useAppStore((state) => state.update);

	useEffect(() => {
		appStoreUpdate(appData);
	}, [appData, appStoreUpdate]);

	useEffect(() => {
		require("bootstrap");
	}, []);

	return (
		<>
			<Header />
			<main className="content">
				<Chat />
				<Breadcrumb />
				{children}
				<Rate />
				{/*<Alerts />*/}
			</main>
			<Footer />
		</>
	);
}
