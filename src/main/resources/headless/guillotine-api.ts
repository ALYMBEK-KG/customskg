import { createSchema, execute } from "/lib/guillotine";

const SCHEMA = createSchema();

export const executeQuery = (query: string, variables?: unknown) => {
	return execute({
		schema: SCHEMA,
		query,
		variables,
	});
};
