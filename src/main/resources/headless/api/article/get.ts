import type { Enonic } from "@enonic/js-utils/types/Request";
import { getList } from "/react4xp/shared/utils/server";

export const get = (request: Enonic.Xp.Http.Request) => {
	try {
		const items = getList({
			contentTypes: ["article"],
			page: !isNaN(parseInt(request.params?.page)) ? parseInt(request.params?.page) : 1,
			filters: [
				{
					hasValue: {
						field: "data.categories",
						values: [request.params?.category],
					},
				},
			],
			sort: [
				{ field: "data.priority", direction: "DESC" },
				{ field: "publish.from", direction: "DESC" },
				{ field: "modifiedTime", direction: "DESC" },
				{ field: "createdTime", direction: "DESC" },
			],
		});

		return {
			status: 200,
			contentType: "application/json",
			body: items,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
