import type { Enonic } from "@enonic/js-utils/types/Request";
import { getUser } from "/lib/xp/auth";
import { get as getContentBy, publish } from "/lib/xp/content";
import { run } from "/lib/xp/context";
import { updateItem } from "/react4xp/shared/utils/server";

export const put = (request: Enonic.Xp.Http.Request) => {
	try {
		const user = getUser();
		const incrementViewed = request.params?.incrementViewed;
		const jsonBody = JSON.parse(request.body ?? "null");
		const existItem = jsonBody?._id ? getContentBy({ key: jsonBody._id }) : null;

		if (existItem == null) {
			return {
				status: 400,
				contentType: "application/json",
			};
		}

		const item = run({ branch: "draft" }, () => {
			if (incrementViewed != null) {
				return updateItem({ _id: existItem._id, data: { viewed: (existItem.data.viewed as number) + 1 } });
			} else if (user != null) {
				return updateItem(jsonBody);
			}

			return null;
		});

		if (item?._id) {
			publish({ keys: [item._id] });
		}

		return {
			status: 200,
			contentType: "application/json",
			body: item,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
