import * as article from "./article";
import * as search from "./search";

export default { article, search };
