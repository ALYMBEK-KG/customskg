import type { Enonic } from "@enonic/js-utils/types/Request";
import api from "./api";

export const all = (request: Enonic.Xp.Http.Request) => {
	const apiPath = request.path
		?.match(new RegExp(/\/api\/.*/))[0]
		.split("/")
		.filter((path) => path);

	return api[apiPath[apiPath.length - 1]][request.method.toLocaleLowerCase()](request);
};
