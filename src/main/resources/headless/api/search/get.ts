import type { Enonic } from "@enonic/js-utils/types/Request";
import { getList } from "/react4xp/shared/utils/server";

export const get = (request: Enonic.Xp.Http.Request) => {
	try {
		const items = getList({
			contentTypes: ["link", "banner", "article"],
			page: !isNaN(parseInt(request.params?.page)) ? parseInt(request.params?.page) : 1,
			query: {
				boolean: {
					must: [
						{
							term: {
								field: "valid",
								value: true,
							},
						},
						{
							term: {
								field: "data.active",
								value: true,
							},
						},
						{
							fulltext: {
								operator: "OR",
								fields: ["displayName", "data.*"],
								query: `*${request.params?.value}*`,
							},
						},
					],
				},
			},
		});

		return {
			status: 200,
			contentType: "application/json",
			body: items,
		};
	} catch (e) {
		return {
			status: 500,
			contentType: "application/json",
			body: e.message,
		};
	}
};
