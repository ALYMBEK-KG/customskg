import { Regions } from "@enonic/react-components";
import React from "react";
import type { ComponentProps } from "./default.d";

import Layout from "../../../react4xp/shared/layouts/default";

function Component(props: ComponentProps) {
	return (
		<Layout {...props}>
			<Regions {...props} />
		</Layout>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
