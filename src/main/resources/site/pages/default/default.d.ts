import type { Region } from "@enonic-types/lib-portal/portal.d";
import type { AppData } from "/react4xp/shared/types/app-data.d";

export interface ComponentProps {
	regionsData: Record<string, Region>;
	names: string | string[];
	tag: string;
	appData: AppData;
}
