import type { ComponentProps } from "./search.d";
import "./search.scss";

import React from "react";
import { useAppStore } from "../../../react4xp/shared/stores";

import Pagination from "../../../react4xp/shared/components/pagination";

function Component(props: ComponentProps) {
	const { items } = props;

	const appStore = useAppStore((state) => state);

	return (
		<div className="global-search-result">
			{items.hits.length > 0 ? (
				<>
					<div className="list-group">
						{items.hits.map(({ _id, displayName, data }) => (
							<a
								key={`global-search-result_${_id}`}
								className="list-group-item list-group-item-action d-flex align-items-center gap-2"
								href={(data?.link as string) ?? (data?.contentTypeLink as string)}
							>
								<span
									className="list-group-item-image flex-shrink-0"
									style={{
										backgroundImage: `url("${data?.contentTypeImage}")`,
									}}
								>
									<span
										className="overlay"
										style={{ backgroundImage: `url("${appStore.imageLinks?.kgOrnament ?? ""}")` }}
									></span>
								</span>
								<span className="">{displayName}</span>
							</a>
						))}
					</div>

					<Pagination total={items.total} />
				</>
			) : (
				<div className="empty">
					<h3>{appStore.phrases?.["empty"]}</h3>
				</div>
			)}
		</div>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
