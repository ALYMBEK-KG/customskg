import type { ComponentProps } from "./search.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent } from "/lib/xp/portal";
import { getList } from "/react4xp/shared/utils/server";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	const items: ComponentProps["items"] = getList({
		contentTypes: ["link", "banner", "article"],
		page: !isNaN(parseInt(request.params?.page)) ? parseInt(request.params?.page) : 1,
		query: {
			boolean: {
				must: [
					{
						term: {
							field: "valid",
							value: true,
						},
					},
					{
						term: {
							field: "data.active",
							value: true,
						},
					},
					{
						fulltext: {
							operator: "OR",
							fields: ["displayName", "data.*"],
							query: `*${request.params?.search}*`,
						},
					},
				],
			},
		},
	});

	const props: ComponentProps = {
		items,
	};

	return render(component, props, request, {
		hydrate: true,
		ssr: true,
	});
}
