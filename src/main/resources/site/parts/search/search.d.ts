import { Content, ContentsResult } from "@enonic-types/lib-content/content.d";

export interface ComponentProps {
	items: ContentsResult<Content>;
}
