import type { ComponentProps } from "./import-chart.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent } from "/lib/xp/portal";
import { get as getImportChart } from "/site/content-types/import-chart/import-chart";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	const items: ComponentProps["items"] = getImportChart({
		pageSize: -1,
	});

	const props: ComponentProps = {
		group: component.config?.group as string,
		items,
	};

	return render(component, props, request, {
		hydrate: false,
		ssr: false,
	});
}
