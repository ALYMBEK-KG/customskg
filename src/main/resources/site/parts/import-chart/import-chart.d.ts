import type { ImportChartDataList } from "/site/content-types/import-chart/import-chart.d";

export interface ComponentProps {
	group: string;
	items: ImportChartDataList;
}
