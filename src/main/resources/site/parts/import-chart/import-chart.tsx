import type { ComponentProps } from "./import-chart.d";
import "./import-chart.scss";

import React from "react";

import { useAppStore } from "../../../react4xp/shared/stores";

import { Form, Formik } from "formik";
import PieChart from "../../../react4xp/shared/components/charts/pie";
import PDFRenderer from "/react4xp/shared/components/pdf-renderer";

function Component(props: ComponentProps) {
	const { group, items } = props;

	const appStore = useAppStore((state) => state);

	const handleSubmit = async (values) => {
		if (values.startDate && values.endDate) {
			// 	ToDo request
		}
	};

	return (
		<section className={`import-chart-${group}`}>
			<Formik
				initialValues={{
					startDate: "",
					endDate: "",
				}}
				onSubmit={handleSubmit}
			>
				{({ values, handleChange, handleBlur, submitForm }) => (
					<Form className="d-flex justify-content-end flex-column flex-sm-row gap-3 mb-2">
						<div className="form-floating">
							<input
								id="startDate"
								name="startDate"
								type="date"
								className="form-control"
								onChange={(e) => {
									handleChange(e);
									submitForm();
								}}
								onBlur={handleBlur}
								value={values.startDate}
							/>
							<label htmlFor="startDate">{appStore.phrases?.["startDate"]}</label>
						</div>
						<div className="form-floating">
							<input
								id="endDate"
								name="endDate"
								type="date"
								className="form-control"
								onChange={(e) => {
									handleChange(e);
									submitForm();
								}}
								onBlur={handleBlur}
								value={values.endDate}
							/>
							<label htmlFor="endDate">{appStore.phrases?.["endDate"]}</label>
						</div>
					</Form>
				)}
			</Formik>

			<div className="chart-container align-items-center gap-2 mt-4">
				{items.hits?.map(({ _id, displayName, data: { values } }) => (
					<PDFRenderer
						key={_id}
						fileName={displayName}
						slots={{
							body: (ref) => (
								<div ref={ref} className="w-100 h-100">
									<div className="chart-title fw-semibold text-center text-black my-2 fs-5">{displayName}</div>
									<div className="chart">{items && <PieChart data={values} />}</div>
								</div>
							),
						}}
					/>
				))}
			</div>

			{items.hits.length === 0 && (
				<div className="empty text-light m-auto">
					<h3>{appStore.phrases?.["empty"]}</h3>
				</div>
			)}
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
