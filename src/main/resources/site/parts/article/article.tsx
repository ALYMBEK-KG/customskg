import type { ComponentProps } from "./article.d";
import "./article.scss";

import type { Content } from "@enonic-types/lib-content/content.d";
import dayjs from "dayjs";
import React, { useCallback, useEffect, useState } from "react";

import Pagination from "../../../react4xp/shared/components/pagination";
import { useEvent } from "../../../react4xp/shared/hooks";
import { useAppStore } from "../../../react4xp/shared/stores";
import { http } from "../../../react4xp/shared/utils/client";
import type { ArticleData, ArticleDataList } from "../../content-types/article/article.d";

function Component(props: ComponentProps) {
	const { tabsType, group, categories } = props;

	const [page, setPage] = useState<number>(1);
	const [category, setCategory] = useState<string>(categories?.hits[0]._id);
	const [loading, setLoading] = useState<boolean>(false);
	const [items, setItems] = useState<ArticleDataList | null>(null);
	const [iframeHtml, setIframeHtml] = useState<string>("");

	const appStore = useAppStore((state) => state);

	const fetchItemsByCategory = useCallback(async () => {
		if (appStore.sitePath == null || appStore.appName == null || category === "") return;

		setItems(null);
		setLoading(true);

		const response = await http({
			url: `api/article?category=${category}&page=${page}`,
		});

		setItems(response.jsonData as ArticleDataList | null);
		setLoading(false);
	}, [appStore.sitePath, appStore.appName, category, page]);

	const incrementItemViews = useCallback(
		(item: Content<ArticleData>) => {
			if (appStore.sitePath == null || appStore.appName == null || item == null) return;

			http({
				url: "api/article?incrementViewed",
				method: "PUT",
				body: JSON.stringify({ _id: item._id }),
			});
		},
		[appStore.sitePath, appStore.appName]
	);

	const handlePageChange = (page: number) => {
		setPage(page);
	};

	useEffect(() => {
		if (categories.hits.length > 0) fetchItemsByCategory();
	}, [categories, fetchItemsByCategory]);

	useEffect(() => {
		if (page === 1 && items?.hits.length === 1) incrementItemViews(items.hits[0]);
	}, [page, items, incrementItemViews]);

	useEvent(
		(e) => {
			if (e.target instanceof HTMLElement) setCategory(e.target.dataset?.id ?? "");
		},
		{ selector: `#article-${group}-tab button`, type: "shown.bs.tab" }
	);

	useEffect(() => {
		const content = items?.hits[0]?.data?.content;

		if (content) {
			const decodedContent = content.replace(/&lt;/g, "<").replace(/&gt;/g, ">");

			const iframeMatch = decodedContent.match(/<iframe[^>]*>[^<]*<\/iframe>/);
			if (iframeMatch && iframeMatch[0]) {
				const cleanedIframeHtml = iframeMatch[0].replace(/\\"/g, '"');
				setIframeHtml(cleanedIframeHtml);
			}
		}
	}, [items?.hits]);

	return (
		<section className={`article article-${group}`}>
			{categories.hits.length > 0 ? (
				<div className={`d-md-flex gap-3 ${tabsType === "vertical" ? "" : "flex-column"}`}>
					<div
						className={`nav nav-pills gap-2 mb-3 mb-md-0 ${tabsType === "vertical" ? "flex-column" : ""}`}
						id={`article-${group}-tab`}
					>
						{categories.hits.map(({ _id, displayName }, i) => (
							<button
								key={`article-${group}-tab-${i}`}
								type="button"
								className={`btn nav-link border ${i === 0 ? "active" : ""}`}
								id={`article-${group}-tab-${i}`}
								data-bs-toggle="pill"
								data-bs-target={`#article-${group}-content-${i}`}
								data-id={_id}
							>
								{displayName}
								<i className={`fa-solid fa-chevron-${tabsType === "vertical" ? "right" : "down"}`}></i>
							</button>
						))}
					</div>

					<div className="tab-content w-100" id={`article-${group}-tabContent`}>
						{categories.hits.map((item, i) => (
							<div
								key={`article-${group}-content-${i}`}
								className={`tab-pane fade w-100 h-100 ${i === 0 ? "show active" : ""}`}
								id={`article-${group}-content-${i}`}
							>
								{items?.hits.length ? (
									items?.hits.length > 1 || page > 1 ? (
										<>
											{items?.hits.map(
												({
													_id,
													displayName,
													modifiedTime,
													publish,
													data: { contentTypeLink, contentTypeImage, contentTypeImageAltText, description, viewed },
												}) => (
													<a
														key={`article-${group}-content-item-${_id}`}
														href={contentTypeLink}
														className="card shadow-sm mb-2"
													>
														<div className="row g-0">
															<div className="col-md-3 rounded overflow-hidden bg-light">
																{contentTypeImage && (
																	<div className="col-md-3 rounded overflow-hidden bg-light w-100 h-100">
																		{contentTypeImage && (
																			<div
																				className="main-image"
																				style={{ backgroundImage: `url(${contentTypeImage})` }}
																				title={contentTypeImageAltText}
																			></div>
																		)}
																	</div>
																)}
															</div>

															<div className="col-md-9">
																<div className="card-body">
																	<h5 className="card-title">{displayName}</h5>

																	<p className="d-flex gap-3 text-muted">
																		<span className="d-flex gap-2">
																			<i className="fa-solid fa-calendar"></i>
																			<small>{dayjs(publish?.from ?? modifiedTime).format("DD.MM.YYYY")}</small>
																		</span>

																		<span className="d-flex gap-2">
																			<i className="fa-solid fa-eye"></i>
																			<small>{`${viewed ?? 0}`}</small>
																		</span>
																	</p>

																	<p className="card-text">{description}</p>
																</div>
															</div>
														</div>
													</a>
												)
											)}

											<Pagination total={items.total} currentPage={page} onPageChange={handlePageChange} />
										</>
									) : (
										<div className="d-flex flex-column gap-2 mx-0 mx-md-2">
											<div className="info">
												<a href={items.hits[0].data.contentTypeLink} className="btn text-start p-0">
													<h3 className="title">{items.hits[0].displayName}</h3>
												</a>

												<p className="d-flex gap-3 text-muted">
													<span className="d-flex gap-2">
														<i className="fa-solid fa-calendar"></i>
														<small>
															{dayjs(items.hits[0].publish?.from ?? items.hits[0].modifiedTime).format("DD.MM.YYYY")}
														</small>
													</span>

													<span className="d-flex gap-2">
														<i className="fa-solid fa-eye"></i>
														<small>{`${items.hits[0].data.viewed ?? 0}`}</small>
													</span>
												</p>
											</div>

											<img
												src={items.hits[0].data.contentTypeImage}
												alt={items.hits[0].data.contentTypeImageAltText}
												loading="lazy"
												className="w-100"
											/>

											<div className="mt-2">
												<div
													dangerouslySetInnerHTML={{
														__html: items.hits[0].data.content,
													}}
												></div>
												{iframeHtml && <div dangerouslySetInnerHTML={{ __html: iframeHtml }} />}
											</div>
										</div>
									)
								) : (
									<div className="empty">
										{loading ? (
											<div className="spinner-border text-primary"></div>
										) : (
											<h3>{appStore.phrases?.["empty"]}</h3>
										)}
									</div>
								)}
							</div>
						))}
					</div>
				</div>
			) : (
				<div className="empty">
					<h3>{appStore.phrases?.["empty"]}</h3>
				</div>
			)}
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
