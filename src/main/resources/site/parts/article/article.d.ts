import { CategoryDataList } from "/site/content-types/category/category.d";

export interface ComponentProps {
	tabsType: string;
	group: string;
	categories: CategoryDataList;
}
