import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent } from "/lib/xp/portal";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	return render(component, {}, request, {
		hydrate: true,
		ssr: true,
	});
}
