import React from "react";

import AtsCalculator from "../../../react4xp/shared/forms/ats-calculator";

function Component() {
	return <AtsCalculator />;
}

export default function WrappedComponent() {
	return <Component />;
}
