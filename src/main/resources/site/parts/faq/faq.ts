import type { ComponentProps } from "./faq.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent } from "/lib/xp/portal";
import { get as getFaq } from "/site/content-types/faq/faq";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	const items: ComponentProps["items"] = getFaq({
		pageSize: -1,
	});

	const props: ComponentProps = {
		items,
	};

	return render(component, props, request, {
		hydrate: true,
		ssr: true,
	});
}
