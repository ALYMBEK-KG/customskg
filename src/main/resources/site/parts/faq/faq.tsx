import type { ComponentProps } from "./faq.d";
import "./faq.scss";

import React, { useEffect, useState } from "react";

import { useAppStore } from "../../../react4xp/shared/stores";

function Component(props: ComponentProps) {
	const { items } = props;

	const appStore = useAppStore((state) => state);

	const [groupedItems, setGroupedItems] = useState<Record<string, ComponentProps["items"]["hits"]> | null>(null);

	useEffect(() => {
		if (items.hits.length > 0) {
			setGroupedItems(
				items.hits.reduce((acc = {}, item) => {
					const key = item.data.category;
					acc[key] ??= [];
					acc[key].push(item);
					return acc;
				}, {})
			);
		}
	}, [items]);

	return (
		<section className="faq">
			{groupedItems ? (
				Object.entries(groupedItems).map(([category, items]) => (
					<div key={category} className="my-2">
						<h5>{category != "undefined" ? category : ""}</h5>

						<div className="accordion" id={`faq-${category}`}>
							{items.map(({ _id, displayName, data: { content } }) => (
								<div key={_id} className="accordion-item">
									<h2 className="accordion-header">
										<button
											type="button"
											data-bs-toggle="collapse"
											data-bs-target={`#${_id}`}
											className="accordion-button collapsed"
										>
											{displayName}
										</button>
									</h2>

									<div id={_id} data-bs-parent={`#faq-${category}`} className="accordion-collapse collapse">
										<div className="accordion-body" dangerouslySetInnerHTML={{ __html: content }}></div>
									</div>
								</div>
							))}
						</div>
					</div>
				))
			) : (
				<div className="empty">
					<h3>{appStore.phrases?.["empty"]}</h3>
				</div>
			)}
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
