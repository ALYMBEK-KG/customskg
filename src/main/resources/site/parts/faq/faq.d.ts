import type { FaqDataList } from "/site/content-types/faq/faq.d";

export interface ComponentProps {
	items: FaqDataList;
}
