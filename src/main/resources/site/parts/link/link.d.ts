import type { LinkDataList } from "/site/content-types/link/link.d";

export interface ComponentProps {
	tabsType: string;
	group: string;
	items: LinkDataList;
}
