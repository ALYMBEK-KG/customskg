import type { ComponentProps } from "./link.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent } from "/lib/xp/portal";
import { get as getLinks } from "/site/content-types/link/link";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	const items: ComponentProps["items"] = getLinks({
		pageSize: -1,
		isAll: true,
		filters: [
			{
				hasValue: {
					field: "data.groups",
					values: [(component.config?.group as string) ?? ""],
				},
			},
		],
	});

	const props: ComponentProps = {
		tabsType: component.config?.tabsType as string,
		group: component.config?.group as string,
		items,
	};

	return render(component, props, request, {
		hydrate: true,
		ssr: true,
	});
}
