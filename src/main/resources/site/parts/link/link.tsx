import type { ComponentProps } from "./link.d";
import "./link.scss";

import React, { useEffect, useState } from "react";

import { useAppStore } from "../../../react4xp/shared/stores";

function Component(props: ComponentProps) {
	const { tabsType, group, items } = props;
	const visibleItemsCount = 6;

	const appStore = useAppStore((state) => state);

	const [showMore, setShowMore] = useState(false);
	const [groupedItems, setGroupedItems] = useState<Record<string, ComponentProps["items"]["hits"]> | null>(null);

	useEffect(() => {
		if (items.hits.length > 0) {
			setGroupedItems(
				items.hits.reduce((acc = {}, item) => {
					const key = item.data.category;
					acc[key] ??= [];
					acc[key].push(item);
					return acc;
				}, {})
			);
		}
	}, [items]);

	return (
		<section className={`link link-${group}`}>
			<div className={`d-md-flex gap-2 ${tabsType === "vertical" ? "" : "flex-column"}`}>
				{groupedItems && (
					<>
						<div
							className={`nav nav-pills gap-2 mb-2 mb-md-0 ${tabsType === "vertical" ? "flex-column" : ""}`}
							id={`link-${group}-tab`}
						>
							{Object.keys(groupedItems).length > 1 &&
								Object.entries(groupedItems).map(([category], i) => (
									<button
										key={`link-${group}-tab-${i}`}
										type="button"
										className={`btn nav-link border ${i === 0 ? "active" : ""}`}
										id={`link-${group}-tab-${i}`}
										data-bs-toggle="pill"
										data-bs-target={`#link-${group}-content-${i}`}
									>
										{category !== "undefined" ? category : appStore.phrases?.["withoutCategory"]}
										<i className={`fa-solid fa-chevron-${tabsType === "vertical" ? "right" : "down"}`}></i>
									</button>
								))}
						</div>

						<div className="tab-content w-100" id={`link-${group}-tabContent`}>
							{Object.entries(groupedItems).map(([, items], i) => (
								<div
									key={`link-${group}-content-${i}`}
									className={`tab-pane fade w-100 h-100 ${i === 0 ? "show active" : ""}`}
									id={`link-${group}-content-${i}`}
								>
									<div className="responsive-grid">
										{items.map(
											(
												{ _id, displayName, data: { contentTypeImage, contentTypeImageAltText, description, link } },
												j
											) => (
												<div
													key={_id}
													className="responsive-grid-item card border-0 shadow-sm"
													style={{
														backgroundImage: `url("${appStore.imageLinks?.kgOrnament ?? ""}")`,
													}}
												>
													<a
														href={link}
														className={`btn flex-row text-start ${
															!showMore && j + 1 > visibleItemsCount ? "d-none" : ""
														}`}
													>
														<div className="overlay"></div>

														<div className="card-body d-flex flex-column gap-3">
															<h4 className="card-title d-flex flex-wrap flex-md-nowrap align-items-center gap-3">
																{contentTypeImage && (
																	<img src={contentTypeImage} alt={contentTypeImageAltText} loading="lazy" />
																)}
																{displayName}
															</h4>

															{description && <p className="h-100 fw-normal">{description}</p>}

															<button type="button" className="btn btn-primary">
																{appStore.phrases?.["moreDetails"]}
																<i className="fa-solid fa-arrow-right-long"></i>
															</button>
														</div>
													</a>
												</div>
											)
										)}
									</div>

									{items.length > visibleItemsCount && (
										<div className="text-center w-100 mt-3">
											<button type="button" className="btn btn-primary" onClick={() => setShowMore((prev) => !prev)}>
												{showMore ? appStore.phrases?.["hide"] : appStore.phrases?.["showAll"]}

												<i className={`fa-solid fa-chevron-${!showMore ? "down" : "up"}`}></i>
											</button>
										</div>
									)}
								</div>
							))}
						</div>
					</>
				)}

				{!groupedItems && (
					<div className="empty">
						<h3>{appStore.phrases?.["empty"]}</h3>
					</div>
				)}
			</div>
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
