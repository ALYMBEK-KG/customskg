import type { ComponentProps } from "./error404.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent, imageUrl } from "/lib/xp/portal";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	const props: ComponentProps = {
		image: imageUrl({
			id: component.config?.image as string,
			scale: "width(1080)",
		}),
		title: component.config?.title as string,
		description: component.config?.description as string,
	};

	return render(component, props, request, {
		hydrate: false,
		ssr: true,
	});
}
