import type { ComponentProps } from "./error404.d";
import "./error404.scss";

import React from "react";

function Component(props: ComponentProps) {
	const { image, title, description } = props;

	return (
		<section className="error404">
			<img src={image} loading="lazy" />
			<h2>{title}</h2>
			<p>{description}</p>
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
