export interface ComponentProps {
	image: string;
	title: string;
	description?: string;
}
