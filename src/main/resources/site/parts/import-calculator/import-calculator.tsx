import React from "react";

import ImportCalculator from "../../../react4xp/shared/forms/import-calculator";

function Component() {
	return <ImportCalculator />;
}

export default function WrappedComponent() {
	return <Component />;
}
