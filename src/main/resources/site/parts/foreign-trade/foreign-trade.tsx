import React from "react";
import type { ComponentProps } from "./foreign-trade.d";

import { Form, Formik } from "formik";
import BarChart from "../../../react4xp/shared/components/charts/bar";
import { useAppStore } from "../../../react4xp/shared/stores";
import PDFRenderer from "/react4xp/shared/components/pdf-renderer";

function Component(props: ComponentProps) {
	const { items } = props;

	const appStore = useAppStore((state) => state);

	const handleSubmit = async (values) => {
		if (values.startDate && values.endDate) {
			// ToDo Request
		}
	};

	return (
		<section className="foreign-trade">
			<Formik
				initialValues={{
					startDate: "",
					endDate: "",
				}}
				onSubmit={handleSubmit}
			>
				{({ values, handleChange, handleBlur, submitForm }) => (
					<Form className="d-flex justify-content-end flex-column flex-sm-row gap-3 mb-2">
						<div className="form-floating">
							<input
								id="startDate"
								name="startDate"
								type="date"
								className="form-control"
								onChange={(e) => {
									handleChange(e);
									submitForm();
								}}
								onBlur={handleBlur}
								value={values.startDate}
							/>
							<label htmlFor="startDate">{appStore.phrases?.["startDate"]}</label>
						</div>
						<div className="form-floating">
							<input
								id="endDate"
								name="endDate"
								type="date"
								className="form-control"
								onChange={(e) => {
									handleChange(e);
									submitForm();
								}}
								onBlur={handleBlur}
								value={values.endDate}
							/>
							<label htmlFor="endDate">{appStore.phrases?.["endDate"]}</label>
						</div>
					</Form>
				)}
			</Formik>

			<div className="chart-container align-items-center gap-2 mt-4">
				{items.hits.length > 0 &&
					items.hits.map(({ _id, displayName, data: { items } }) => {
						return (
							<PDFRenderer
								key={_id}
								fileName={displayName}
								slots={{
									body: (ref) => (
										<div ref={ref} className="w-100 h-100">
											<div className="chart-title fw-semibold text-center text-black my-2 fs-5">{displayName}</div>
											<div className="chart">{items && <BarChart data={items} />}</div>
										</div>
									),
								}}
							/>
						);
					})}
			</div>

			{items.hits.length === 0 && (
				<div className="empty">
					<h3>{appStore.phrases?.["empty"]}</h3>
				</div>
			)}
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
