import type { ForeignTradeDataList } from "/site/content-types/foreign-trade/foreign-trade.d";

export interface ComponentProps {
	items: ForeignTradeDataList;
}
