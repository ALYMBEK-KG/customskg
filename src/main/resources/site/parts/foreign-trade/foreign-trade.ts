import type { ComponentProps } from "./foreign-trade.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent } from "/lib/xp/portal";
import { get as getForeignTrades } from "/site/content-types/foreign-trade/foreign-trade";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	const items: ComponentProps["items"] = getForeignTrades({
		pageSize: -1,
	});

	const props: ComponentProps = {
		items,
	};

	return render(component, props, request, {
		hydrate: false,
		ssr: false,
	});
}
