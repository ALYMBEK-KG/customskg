import type { ComponentProps } from "./statistics.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent } from "/lib/xp/portal";
import { get as getStatistics } from "/site/content-types/statistics/statistics";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	const items: ComponentProps["items"] = getStatistics({
		pageSize: -1,
	});

	const props: ComponentProps = {
		tabsType: component.config?.tabsType as string,
		group: component.config?.group as string,
		items,
	};

	return render(component, props, request, {
		hydrate: true,
		ssr: true,
	});
}
