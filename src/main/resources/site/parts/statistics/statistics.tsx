import type { ComponentProps } from "./statistics.d";
import "./statistics.scss";

import React, { useState } from "react";

import { useAppStore } from "../../../react4xp/shared/stores";

import dayjs from "dayjs";
import AreaChart from "../../../react4xp/shared/components/charts/area";
import type { AreaChartItem } from "../../../react4xp/shared/components/charts/area/index.d";
import PieChart from "../../../react4xp/shared/components/charts/pie";

function Component(props: ComponentProps) {
	const { tabsType, group, items } = props;
	const [chartType, setChartType] = useState<"area" | "pie">("area");

	const appStore = useAppStore((state) => state);

	const toggleChartType = () => {
		setChartType((prev) => (prev === "area" ? "pie" : "area"));
	};

	return (
		<section className={`statistics statistics-${group}`}>
			<div className={`d-md-flex gap-2 ${tabsType === "vertical" ? "" : "flex-column"}`}>
				{items.hits.length > 0 && (
					<>
						<div
							className={`nav nav-pills gap-2 mb-2 mb-md-0 ${
								tabsType === "vertical" ? "flex-column" : ""
							} ${items.hits.length <= 1 ? "d-none" : ""}`}
							id={`statistics-${group}-tab`}
						>
							{items.hits.map(({ _id, displayName, data: { icon } }, i) => {
								return (
									<button
										key={`statistics-${group}-tab-${_id}`}
										type="button"
										className={`btn nav-link border ${i === 0 ? "active" : ""}`}
										id={`statistics-${group}-tab-${_id}`}
										data-bs-toggle="pill"
										data-bs-target={`#statistics-${group}-content-${_id}`}
									>
										{icon && <i className={icon}></i>}
										{displayName}
										<i className={`fa-solid fa-chevron-${tabsType === "vertical" ? "right" : "down"}`}></i>
									</button>
								);
							})}
						</div>

						<div className="tab-content w-100" id={`statistics-${group}-tabContent`}>
							<div className="d-flex justify-content-end">
								<button className="btn btn-outline-primary" onClick={toggleChartType}>
									{chartType === "area" ? (
										<i className="fa-solid fa-chart-pie"></i>
									) : (
										<i className="fa-solid fa-chart-area"></i>
									)}
								</button>
							</div>
							{items.hits.map(({ _id, displayName, data: { values } }, i) => {
								let chartData: AreaChartItem[] = [];

								if (Array.isArray(values)) {
									const data: Record<string, AreaChartItem> = values.reduce((a, v) => {
										const date = dayjs(v.date);
										const name = chartType === "area" ? date.format("MMMM") : v.name;

										if (a[name] != null) {
											a[name].value += v.value;
										} else {
											a[name] = {
												name,
												date: date.format("YYYY-MM-DD"),
												value: v.value,
											};
										}

										return a;
									}, {});

									chartData = Object.values(data).sort((a, b) => {
										const date1 = dayjs(a.date);
										const date2 = dayjs(b.date);

										if (date1.isBefore(date2)) return -1;
										if (date1.isAfter(date2)) return 1;

										return 0;
									});
								} else {
									const date = dayjs(values.date);

									chartData.push({
										name: date.format("MMMM"),
										date: date.format("YYYY-MM-DD"),
										value: values.value,
									});
								}

								return (
									<div
										key={`statistics-${group}-content-${_id}`}
										className={`tab-pane fade w-100 h-100 ${i === 0 ? "show active" : ""}`}
										id={`statistics-${group}-content-${_id}`}
									>
										<div className="chart-title fw-semibold text-center text-primary my-1">{displayName}</div>

										<div className="chart">
											{chartData && chartType === "area" ? (
												<AreaChart data={chartData} />
											) : (
												<PieChart data={chartData} />
											)}
										</div>
									</div>
								);
							})}
						</div>
					</>
				)}

				{items.hits.length === 0 && (
					<div className="empty">
						<h3>{appStore.phrases?.["empty"]}</h3>
					</div>
				)}
			</div>
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
