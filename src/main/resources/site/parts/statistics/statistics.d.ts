import type { StatisticsDataList } from "/site/content-types/statistics/statistics.d";

export interface ComponentProps {
	tabsType: string;
	group: string;
	items: StatisticsDataList;
}
