import type { ComponentProps } from "./last-news.d";
import "./last-news.scss";

import dayjs from "dayjs";
import React from "react";

import { useAppStore } from "../../../react4xp/shared/stores";

function Component(props: ComponentProps) {
	const { group, items } = props;

	const appStore = useAppStore((state) => state);

	return (
		<section className={`last-news last-news-${group}`}>
			{items.hits.length > 0 ? (
				<>
					<div className="d-flex flex-column flex-md-row gap-3">
						<div
							id="lastNewsCarousel"
							className="carousel slide d-flex flex-column justify-content-between bg-white rounded shadow-sm p-4"
							data-bs-ride="carousel"
						>
							<div className="carousel-inner">
								{items.hits.map(
									({ displayName, publish, modifiedTime, data: { contentTypeLink, description, viewed } }, i) => (
										<div key={`carousel-item-${i}`} className={`carousel-item ${i === 0 ? "active" : ""}`}>
											<h3>{displayName}</h3>

											<p className="d-flex gap-3 text-muted">
												<span className="d-flex gap-2">
													<i className="fa-solid fa-calendar"></i>
													<small>{dayjs(publish?.from ?? modifiedTime).format("DD.MM.YYYY")}</small>
												</span>

												<span className="d-flex gap-2">
													<i className="fa-solid fa-eye"></i>
													<small>{`${viewed ?? 0}`}</small>
												</span>
											</p>

											<p>{description}</p>

											<a href={contentTypeLink} className="d-flex align-items-center gap-2 text-decoration-none">
												{appStore.phrases?.["moreDetails"]}
												<i className="fa-solid fa-arrow-right-long"></i>
											</a>
										</div>
									)
								)}
							</div>

							<div className="d-flex align-items-center justify-content-between mt-3">
								<button
									type="button"
									className="carousel-control-prev"
									data-bs-target="#lastNewsCarousel"
									data-bs-slide="prev"
								>
									<i className="fa-solid fa-arrow-left-long"></i>
								</button>

								<button
									type="button"
									className="carousel-control-next"
									data-bs-target="#lastNewsCarousel"
									data-bs-slide="next"
								>
									<i className="fa-solid fa-arrow-right-long"></i>
								</button>
							</div>
						</div>

						<div className="main-news rounded">
							<div className="image" style={{ backgroundImage: `url("${items.hits[0].data.contentTypeImage}")` }}></div>

							<div
								className="overlay"
								style={{
									backgroundImage: `url("${appStore.imageLinks?.kgOrnament ?? ""}")`,
								}}
							></div>

							<div className="content">
								<h3>{items.hits[0].displayName}</h3>

								<p className="d-flex gap-3">
									<span className="d-flex gap-2">
										<i className="fa-solid fa-calendar"></i>
										<small>
											{dayjs(items.hits[0].publish?.from ?? items.hits[0].modifiedTime).format("DD.MM.YYYY")}
										</small>
									</span>

									<span className="d-flex gap-2">
										<i className="fa-solid fa-eye"></i>
										<small>{`${items.hits[0].data.viewed ?? 0}`}</small>
									</span>
								</p>

								<p>{items.hits[0].data.description}</p>

								<a href={items.hits[0].data.contentTypeLink} className="btn btn-primary">
									{appStore.phrases?.["moreDetails"]}
									<i className="fa-solid fa-arrow-right-long"></i>
								</a>
							</div>
						</div>
					</div>

					{items?.hits.map(
						({
							_id,
							displayName,
							modifiedTime,
							publish,
							data: { contentTypeLink, contentTypeImage, contentTypeImageAltText, description, viewed },
						}) => (
							<a key={`article-${group}-content-item-${_id}`} href={contentTypeLink} className="card shadow-sm mb-2">
								<div className="row g-0">
									<div className="col-md-3 rounded overflow-hidden bg-light">
										{contentTypeImage && (
											<div
												className="main-image"
												style={{ backgroundImage: `url(${contentTypeImage})` }}
												title={contentTypeImageAltText}
											></div>
										)}
									</div>

									<div className="col-md-9">
										<div className="card-body">
											<h5 className="card-title">{displayName}</h5>

											<p className="d-flex gap-3 text-muted">
												<span className="d-flex gap-2">
													<i className="fa-solid fa-calendar"></i>
													<small>{dayjs(publish?.from ?? modifiedTime).format("DD.MM.YYYY")}</small>
												</span>

												<span className="d-flex gap-2">
													<i className="fa-solid fa-eye"></i>
													<small>{`${viewed ?? 0}`}</small>
												</span>
											</p>

											<p className="card-text">{description}</p>
										</div>
									</div>
								</div>
							</a>
						)
					)}

					<a href={`${appStore.sitePath?.[0].link}/news`} className="btn btn-primary m-auto">
						{appStore.phrases?.["showAll"]}
					</a>
				</>
			) : (
				<div className="empty">
					<h3>{appStore.phrases?.["empty"]}</h3>
				</div>
			)}
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
