import type { ComponentProps } from "./last-news.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { get as getContentBy } from "/lib/xp/content";
import { getComponent, getContent } from "/lib/xp/portal";
import { get as getNews } from "/site/content-types/article/article";
import { get as getCategories } from "/site/content-types/category/category";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();
	const content = getContent();
	const group = component.config?.group ? getContentBy({ key: component.config?.group as string }) : null;
	const pathMatch = `/content${group?._path ?? content._path}/*`;
	const categories = getCategories({
		pageSize: -1,
		query: {
			boolean: {
				must: [
					{
						term: {
							field: "valid",
							value: true,
						},
					},
					{
						term: {
							field: "data.active",
							value: true,
						},
					},
					{
						pathMatch: {
							field: "_path",
							path: pathMatch,
							minimumMatch: pathMatch.split("/").filter((path) => path).length - 1,
						},
					},
				],
			},
		},
		filters: [{ boolean: { mustNot: { ids: { values: [group?._id] } } } }],
	});

	const items: ComponentProps["items"] = getNews({
		pageSize: 5,
		isAll: true,
		filters: [
			{
				boolean: {
					must: {
						hasValue: {
							field: "data.categories",
							values: categories.hits.map((item) => item._id),
						},
					},
				},
			},
		],
		sort: [
			{ field: "data.priority", direction: "DESC" },
			{ field: "publish.from", direction: "DESC" },
			{ field: "modifiedTime", direction: "DESC" },
			{ field: "createdTime", direction: "DESC" },
		],
	});

	const props: ComponentProps = {
		group: component.config?.group as string,
		items,
	};

	return render(component, props, request, {
		hydrate: true,
		ssr: true,
	});
}
