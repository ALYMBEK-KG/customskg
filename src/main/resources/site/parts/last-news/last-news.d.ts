import type { ArticleDataList } from "/site/content-types/article/article.d";

export interface ComponentProps {
	group: string;
	items: ArticleDataList;
}
