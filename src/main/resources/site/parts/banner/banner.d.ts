import type { BannerDataList } from "/site/content-types/banner/banner.d";

export interface ComponentProps {
	group: string;
	items: BannerDataList;
}
