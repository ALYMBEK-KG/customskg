import type { ComponentProps } from "./banner.d";
import "./banner.scss";

import React, { useEffect } from "react";

import { useAppStore } from "../../../react4xp/shared/stores";

function Component(props: ComponentProps) {
	const { group, items } = props;

	const appStore = useAppStore((state) => state);
	const appStoreUpdate = useAppStore((state) => state.update);

	useEffect(() => {
		appStoreUpdate({ isHeaderTransparent: true });
	}, [appStoreUpdate]);

	return (
		<section className={`banner banner-${group}`}>
			{items.hits.length > 0 && (
				<div id="banner-carousel" className="carousel slide" data-bs-ride="carousel">
					<div className="carousel-indicators">
						<div className="container d-flex align-items-center gap-1">
							<button
								type="button"
								className="custom-carousel-control-prev btn"
								data-bs-target="#banner-carousel"
								data-bs-slide="prev"
							>
								<i className="fa-solid fa-arrow-left-long"></i>
							</button>

							{items.hits.map((url, i) => (
								<button
									key={`slide-indicator-${i}`}
									type="button"
									className={`carousel-control-indicator ${i === 0 ? "active" : ""}`}
									data-bs-target="#banner-carousel"
									data-bs-slide-to={i}
								></button>
							))}

							<button
								type="button"
								className="custom-carousel-control-next btn"
								data-bs-target="#banner-carousel"
								data-bs-slide="next"
							>
								<i className="fa-solid fa-arrow-right-long"></i>
							</button>
						</div>
					</div>

					<div className="carousel-inner">
						{items.hits.map(({ displayName, data: { contentTypeImage, description, link } }, i) => (
							<div
								key={`slide-${i}`}
								className={`carousel-item ${i === 0 ? "active" : ""}`}
								style={{ backgroundImage: `url("${contentTypeImage}")` }}
							>
								<div className="overlay">
									<div className="container">
										<h1 className="title text-light text-uppercase">{displayName}</h1>
										<p>{description}</p>
										{link && (
											<a className="btn btn-primary" href={link}>
												{appStore.phrases?.["moreDetails"]}
												<i className="fa-solid fa-arrow-right-long"></i>
											</a>
										)}
									</div>
								</div>
							</div>
						))}
					</div>
				</div>
			)}

			{items.hits.length === 0 && (
				<div className="empty text-light m-auto">
					<h3>{appStore.phrases?.["empty"]}</h3>
				</div>
			)}
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
