import type { ComponentProps } from "./banner.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent } from "/lib/xp/portal";
import { get as getBanners } from "/site/content-types/banner/banner";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	const items: ComponentProps["items"] = getBanners({
		pageSize: -1,
	});

	const props: ComponentProps = {
		group: component.config?.group as string,
		items,
	};

	return render(component, props, request, {
		hydrate: true,
		ssr: true,
	});
}
