import type { ComponentProps } from "./map.d";

import type { PartComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent, processHtml } from "/lib/xp/portal";
import { get as getMaps } from "/site/content-types/map/map";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<PartComponent>();

	const items: ComponentProps["items"] = getMaps({
		pageSize: -1,
		onEveryItem: (item) => {
			const markers = item.data.markers as Record<string, unknown> | Record<string, unknown>[];
			const polygons = item.data.polygons as Record<string, unknown> | Record<string, unknown>[];

			if (Array.isArray(markers)) {
				item.data.markers = markers.map((v) => {
					if (v?.popup) v.popup = processHtml({ value: v?.popup as string });
					return v;
				});
			} else if (markers?.popup) {
				item.data.markers = { ...markers, popup: processHtml({ value: markers.popup as string }) };
			}

			if (Array.isArray(polygons)) {
				item.data.polygons = polygons.map((v) => {
					if (v?.popup) v.popup = processHtml({ value: v?.popup as string });
					return v;
				});
			} else if (polygons?.popup) {
				item.data.polygons = { ...polygons, popup: processHtml({ value: polygons.popup as string }) };
			}
		},
	});

	const props: ComponentProps = {
		tabsType: component.config?.tabsType as string,
		group: component.config?.group as string,
		items,
	};

	return render(component, props, request, {
		hydrate: true,
		ssr: false,
	});
}
