import type { MapDataList } from "/site/content-types/map/map.d";

export interface ComponentProps {
	tabsType: string;
	group: string;
	items: MapDataList;
}
