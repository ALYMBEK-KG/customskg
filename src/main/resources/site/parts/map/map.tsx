import type { ComponentProps } from "./map.d";
import "./map.scss";

import React from "react";

import { useAppStore } from "../../../react4xp/shared/stores";

import MapContainer from "../../../react4xp/shared/components/map";

function Component(props: ComponentProps) {
	const { tabsType, group, items } = props;

	const appStore = useAppStore((state) => state);

	return (
		<section className={`map map-${group}`}>
			<div className={`d-md-flex gap-2 ${tabsType === "vertical" ? "" : "flex-column"}`}>
				{items.hits.length > 0 && (
					<>
						<div
							className={`nav nav-pills gap-2 mb-2 mb-md-0 ${tabsType === "vertical" ? "flex-column" : ""}`}
							id={`map-${group}-tab`}
						>
							{items.hits.length > 1 &&
								items.hits.map((item, i) => (
									<button
										key={`map-${group}-tab-${item._id}`}
										type="button"
										className={`btn nav-link border ${i === 0 ? "active" : ""}`}
										id={`map-${group}-tab-${item._id}`}
										data-bs-toggle="pill"
										data-bs-target={`#map-${group}-content-${item._id}`}
									>
										{item.data.icon && <i className={item.data.icon}></i>}
										{item.displayName}
										<i className={`fa-solid fa-chevron-${tabsType === "vertical" ? "right" : "down"}`}></i>
									</button>
								))}
						</div>

						<div className="tab-content w-100" id={`map-${group}-tabContent`}>
							{items.hits.map((item, i) => (
								<div
									key={`map-${group}-content-${item._id}`}
									className={`tab-pane fade w-100 h-100 ${i === 0 ? "show active" : ""}`}
									id={`map-${group}-content-${item._id}`}
								>
									<MapContainer
										mapId={`map-${group}-container-${item._id}`}
										markers={
											Array.isArray(item.data.markers)
												? item.data.markers.map((m) => ({
														coordinates: m.coordinates.split(","),
														popup: m?.popup,
													}))
												: [
														{
															coordinates: item.data.markers.coordinates.split(","),
															popup: item.data.markers?.popup,
														},
													]
										}
										polygons={
											Array.isArray(item.data.polygons)
												? item.data.polygons.map((p) => ({
														geoJson: JSON.parse(p?.geoJson ?? "null"),
														popup: p?.popup,
													}))
												: [
														{
															geoJson: JSON.parse(item.data.polygons.geoJson ?? "null"),
															popup: item.data.polygons?.popup,
														},
													]
										}
										onInit={(map) => {
											document.querySelectorAll(`#map-${group}-tab button`)?.forEach((item) =>
												item.addEventListener("click", () => {
													map.invalidateSize();
												})
											);
										}}
									/>
								</div>
							))}
						</div>
					</>
				)}

				{items.hits.length === 0 && (
					<div className="empty">
						<h3>{appStore.phrases?.["empty"]}</h3>
					</div>
				)}
			</div>
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
