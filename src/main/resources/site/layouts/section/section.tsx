import type { ComponentProps } from "./section.d";
import "./section.scss";

import { Regions } from "@enonic/react-components";
import React from "react";

function Component(props: ComponentProps) {
	const { settings } = props;

	return (
		<section
			className={`section-layout ${settings.classes ?? ""}`}
			style={{ backgroundImage: `url("${settings.backgroundImage ?? ""}")` }}
		>
			<div className="container">
				<div className="row">
					<div className="col-12">
						{settings.title && <h2 className="title">{settings.title}</h2>}
						{settings.description && <p>{settings.description}</p>}
						<Regions {...props} />
					</div>
				</div>
			</div>
		</section>
	);
}

export default function WrappedComponent(props: ComponentProps) {
	return <Component {...props} />;
}
