import type { Region } from "@enonic-types/lib-portal/portal.d";

export interface ComponentProps {
	regionsData: Record<string, Region>;
	names: string | string[];
	tag: string;
	settings: {
		title?: string;
		description?: string;
		backgroundImage?: string;
		classes?: string;
	};
}
