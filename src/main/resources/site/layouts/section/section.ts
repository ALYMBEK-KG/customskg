import type { ComponentProps } from "./section.d";

import type { LayoutComponent } from "@enonic-types/core";
import type { Enonic } from "@enonic/js-utils/types/Request";

import { render } from "/lib/enonic/react4xp";
import { getComponent, imageUrl } from "/lib/xp/portal";

export function get(request: Enonic.Xp.Http.Request) {
	const component = getComponent<LayoutComponent>();
	const headTags = [];
	const props: ComponentProps = {
		regionsData: component.regions,
		names: "main",
		tag: "main",
		settings: {
			title: (component.config?.title as string) ?? "",
			description: (component.config?.description as string) ?? "",
			backgroundImage: component.config?.backgroundImage
				? imageUrl({
						id: component.config.backgroundImage as string,
						scale: "width(1080)",
					})
				: "",
			classes: (component.config?.classes as string) ?? "",
		},
	};

	if (component.config?.styles) headTags.push(`<style>${component.config.styles}</style>`);

	return render(component, props, request, {
		hydrate: false,
		ssr: true,
		pageContributions: {
			headEnd: headTags,
		},
	});
}
