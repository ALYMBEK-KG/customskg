import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import type { ContentType } from "/react4xp/shared/types/content-type.d";

export interface MapMarker {
	coordinates: string;
	popup?: string;
}

export interface MapPolygon {
	countryCode: string;
	geoJson?: string;
	popup?: string;
}

export type MapData = ContentType & {
	active?: boolean;
	markers: MapMarker | MapMarker[];
	polygons: MapPolygon | MapPolygon[];
};

export type MapDataList = ContentsResult<Content<MapData>>;
