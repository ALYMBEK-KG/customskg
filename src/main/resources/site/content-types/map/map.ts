import type { MapData, MapDataList } from "./map.d";

import { GetListOptions, getList } from "/react4xp/shared/utils/server";

export function get(options?: Omit<GetListOptions, "contentTypes">): MapDataList {
	const items = getList<MapData>({ contentTypes: ["map"], ...options });

	return items;
}
