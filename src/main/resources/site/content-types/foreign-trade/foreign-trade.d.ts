import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import type { ContentType } from "/react4xp/shared/types/content-type.d";

export type ForeignTradeValue = {
	date: string;
	value: number;
};

export type ForeignTradeValues = {
	name: string;
	values: ForeignTradeValue[];
};

export type ForeignTradeData = ContentType & {
	icon?: string;
	active?: boolean;
	items: ForeignTradeValues[];
};

export type ForeignTradeDataList = ContentsResult<Content<ForeignTradeData>>;
