import type { ForeignTradeData, ForeignTradeDataList } from "./foreign-trade.d";

import { GetListOptions, getList } from "/react4xp/shared/utils/server";

export function get(options?: Omit<GetListOptions, "contentTypes">): ForeignTradeDataList {
	const items = getList<ForeignTradeData>({ contentTypes: ["foreign-trade"], ...options });

	return items;
}
