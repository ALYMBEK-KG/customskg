import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import type { ContentType } from "/react4xp/shared/types/content-type.d";

export type BannerData = ContentType & {
	description?: string;
	link?: string;
	active?: boolean;
};

export type BannerDataList = ContentsResult<Content<BannerData>>;
