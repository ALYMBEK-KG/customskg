import type { BannerData, BannerDataList } from "./banner.d";

import { GetListOptions, getList } from "/react4xp/shared/utils/server";

export function get(options?: Omit<GetListOptions, "contentTypes">): BannerDataList {
	const items = getList<BannerData>({ contentTypes: ["banner"], ...options });

	return items;
}
