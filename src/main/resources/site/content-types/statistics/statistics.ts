import type { StatisticsData, StatisticsDataList } from "./statistics.d";

import { GetListOptions, getList } from "/react4xp/shared/utils/server";

export function get(options?: Omit<GetListOptions, "contentTypes">): StatisticsDataList {
	const items = getList<StatisticsData>({ contentTypes: ["statistics"], ...options });

	return items;
}
