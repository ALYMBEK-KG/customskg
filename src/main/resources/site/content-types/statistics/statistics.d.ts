import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import type { ContentType } from "/react4xp/shared/types/content-type.d";

export type StatisticsValue = {
	date: string;
	value: number;
	name?: string;
};

export type StatisticsData = ContentType & {
	icon?: string;
	active?: boolean;
	values: StatisticsValue | StatisticsValue[];
};

export type StatisticsDataList = ContentsResult<Content<StatisticsData>>;
