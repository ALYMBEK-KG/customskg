import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import type { ContentType } from "/react4xp/shared/types/content-type.d";

export type FaqData = ContentType & {
	category?: string;
	answer?: string;
	active?: boolean;
};

export type FaqDataList = ContentsResult<Content<FaqData>>;
