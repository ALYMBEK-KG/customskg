import type { FaqData, FaqDataList } from "./faq.d";

import { GetListOptions, getList } from "/react4xp/shared/utils/server";

export function get(options?: Omit<GetListOptions, "contentTypes">): FaqDataList {
	const items = getList<FaqData>({ contentTypes: ["faq"], ...options });

	return items;
}
