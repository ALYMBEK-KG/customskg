import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import type { ContentType } from "/react4xp/shared/types/content-type.d";

export type LinkData = ContentType & {
	groups: string;
	link: string;
	category?: string;
	description?: string;
	active?: boolean;
};

export type LinkDataList = ContentsResult<Content<LinkData>>;
