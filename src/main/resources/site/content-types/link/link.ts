import type { LinkData, LinkDataList } from "./link.d";

import { GetListOptions, getList } from "/react4xp/shared/utils/server";

export function get(options?: Omit<GetListOptions, "contentTypes">): LinkDataList {
	const items = getList<LinkData>({ contentTypes: ["link"], ...options });

	return items;
}
