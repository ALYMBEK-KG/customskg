import type { ArticleData, ArticleDataList } from "./article.d";

import { GetListOptions, getList } from "/react4xp/shared/utils/server";

export function get(options?: Omit<GetListOptions, "contentTypes">): ArticleDataList {
	const items = getList<ArticleData>({ contentTypes: ["article"], ...options });

	return items;
}
