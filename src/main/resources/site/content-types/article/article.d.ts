import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import type { ContentType } from "/react4xp/shared/types/content-type.d";

export type ArticleData = ContentType & {
	categories: string[];
	description?: string;
	content?: string;
	viewed?: number;
	active?: boolean;
};

export type ArticleDataList = ContentsResult<Content<ArticleData>>;
