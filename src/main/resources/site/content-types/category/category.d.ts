import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import type { ContentType } from "/react4xp/shared/types/content-type.d";

export type CategoryData = ContentType & {
	active?: boolean;
};

export type CategoryDataList = ContentsResult<Content<CategoryData>>;
