import type { CategoryData, CategoryDataList } from "./category.d";

import { GetListOptions, getList } from "/react4xp/shared/utils/server";

export function get(options?: Omit<GetListOptions, "contentTypes">): CategoryDataList {
	const items = getList<CategoryData>({ contentTypes: ["category"], ...options });

	return items;
}
