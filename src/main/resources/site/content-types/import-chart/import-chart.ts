import type { ImportChartData, ImportChartDataList } from "./import-chart.d";

import { GetListOptions, getList } from "/react4xp/shared/utils/server";

export function get(options?: Omit<GetListOptions, "contentTypes">): ImportChartDataList {
	const items = getList<ImportChartData>({ contentTypes: ["import-chart"], ...options });

	return items;
}
