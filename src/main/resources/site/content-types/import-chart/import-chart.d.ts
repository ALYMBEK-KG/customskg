import type { Content, ContentsResult } from "@enonic-types/lib-content/content.d";
import type { ContentType } from "/react4xp/shared/types/content-type.d";

export type ImportChartValue = {
	name: string;
	value: number;
};

export type ImportChartData = ContentType & {
	active?: boolean;
	values: ImportChartValue | ImportChartValue[];
};

export type ImportChartDataList = ContentsResult<Content<ImportChartData>>;
