import type { RouterRequest } from "@item-enonic-types/lib-router";
import initRouter from "/lib/router";
import { getMimeType, getResource } from "/lib/xp/io";

const router = initRouter();

router.get("{path:.*}", function (request: RouterRequest) {
	const file = resolve(`./${request.pathParams?.path}`);
	const mimeType = getMimeType(file.getPath());
	const resource = getResource(file);

	return {
		body: resource.getStream(),
		contentType: `${mimeType}; charset=utf-8`,
	};
});

export const get = function (request: RouterRequest) {
	return router.dispatch(request);
};
