# CustomsKG with [React4XP](https://developer.enonic.com/docs/react4xp/stable) and [Enonic XP](https://developer.enonic.com/docs/xp/stable/framework)

## Getting started

Run the commands below to run this app:

```
npm install -g @enonic/cli
git clone https://gitlab.com/ALYMBEK-KG/customskg.git
cd customskg
enonic dev
```

Finally open the link in the browser: http://localhost:8080
